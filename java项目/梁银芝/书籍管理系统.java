package Demo;

import java.text.BreakIterator;
import java.util.Scanner;

public class Demo01 {
	static String[][] users = new String[50][5];
	static String[][] books = new String[50][7];
	static String[][] pubs = new String[50][5];
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		init();
		
		while (true) {
			System.out.println("欢迎来到闽大图书管理系统！");
			System.out.println("1登录  2注册");
			int key = sc.nextInt();

			switch (key) {
			// 登录
			case 1:
				loginModule();
				MIB();
				break;
			// 注册
			// 需要一个null的索引
			case 2:
				registerModule();
			default:
				break;
			}

		}

	}
    //注册
	private static void registerModule() {
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入所属部门：");
		users[index][0] = sc.next();
		System.out.println("请输入用户名：");
		users[index][1] = sc.next();
		System.out.println("请输入密码：");
		users[index][2] = sc.next();
		users[index][3] = "普通用户";

		System.out.println("注册成功！");

		
	}
    //登录
	private static void loginModule() {
		for (int i = 0; i < 3; i++) {
			System.out.println("请输入用户名：");
			String usresname = sc.next();
			System.out.println("请输入登录密码：");
			String password = sc.next();

			boolean flag = login(usresname, password);
			if (flag) {
				System.out.println("登录成功！");
				break;
			} else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
		}
		System.out.println("三次输入错误，系统自动退出!");
		
	}

	private static boolean login(String usresname, String password) {
		boolean result = false;

		for (int i = 0; i < users.length; i++) {
			if (usresname.equals(users[i][1]) && password.equals(users[i][2])) {
				result = true;
			}
		}
		return result;
	}

	public static void MIB() {
		System.out.println("欢迎使用闽大图书管理系统！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出系统");
		int k1 = sc.nextInt();
		switch (k1) {
		// 图书管理
		case 1:
			bookManage();
			break;
		// 出版社管理
		//1.增加
		//2.删除：出版社有关联的图书不能删除；
		//3.更新 
		//4.根据出版社名称查询
		//5.查询所有出版社
	    //6.返回上一级菜单
		case 2:
			pressManage();
			break;
		// 退出系统
		case 3:
			System.out.println("系统退出成功！");
			System.exit(0);
			break;

		default:
			break;
		}
		
	}
	
	//出版社管理
	private static void pressManage() {
		System.out.println("1.增加2.删除：出版社有关联的图书不能删除3.更新4.根据出版社名称查询5.查询所有出版社 6.返回上一级菜单");
		int k3=sc.nextInt();
		switch (k3) {
		case 1:
		//增加	
		int index = -1;
		for (int i = 0; i < pubs.length; i++) {
			if (pubs[i][0] == null) {
				index = i;
				System.out.println("请输入出版社名称：");
				pubs [index][0] = sc.next();
				System.out.println("请输入地址：");
				pubs [index][1] = sc.next();
				System.out.println("请输入联系人：");
				pubs [index][2] = sc.next();
				
				System.out.println("增加成功！");
				pressManage();
				break;
			}
		}
		//删除：出版社有关联的图书不能删除
		case 2:
			System.out.println("请输入要删除的出版社：");
			String chubanshe = sc.next();
			for (int i = 0; i < pubs.length; i++) {
				if (chubanshe.equals(pubs[i][0])) {
					pubs [i][0]=null;
					pubs [i][1]=null;
					pubs [i][2]=null;
					System.out.println("删除成功！");
				}
			}
			
			pressManage();
			break;
		//更新	
		case 3:
			System.out.println("请输入出版社名称:");
			String bianhao = sc.next();
			index = -1;
			for (int i = 0; i < pubs.length; i++) {
				if (bianhao.equals(pubs[index][0])) {
					index=i;	
					System.out.println("出版社名称:");
					pubs [i][0] = sc.next();
					System.out.println("地址:");
					pubs [i][1] = sc.next();
					System.out.println("联系人:");
					pubs [i][2] = sc.next();
					
					System.out.println("更新成功！");
					pressManage();
			break;
				}
				
			}
		//根据出版社名称查询	
		case 4:
			System.out.println("请输入出版社名称：");
			String chu = sc.next();
			System.out.println("出版社名称"+"\t"+"地址"+"\t"+"联系人");
			for (int i = 0; i < pubs.length; i++) {
				if (chu.equals(pubs[i][0])) {
					System.out.println(pubs[i][0]+"\t"+pubs[i][1]+"\t"+pubs[i][2]);
					
					pressManage();
					
				}
			}
			break;
		//查询所有出版社 	
		case 5:
			chuManage();
			pressManage();
			break;
		//返回上一级菜单	
		case 6:
			MIB();
			break;

	}
	}
	
	private static void chuManage() {
		System.out.println("出版社名称"+"\t"+"地址"+"\t"+"联系人");
		for (int i = 0; i < pubs.length; i++) {
			if (pubs[i][0]!=null) {
				System.out.println(pubs[i][0]+"\t"+pubs[i][1]+"\t"+pubs[i][2]);
			}
		}
		
	}
	//图书管理
	private static void bookManage() {
		System.out.println("请输入：1.增加2.删除3.更新4.查询5.返回上一级菜单");
		int k2 = sc.nextInt();
		switch (k2) {
		// 增加
		case 1:
			int index = -1;
			for (int i = 0; i < books.length; i++) {
				if (books[i][0] == null) {
					index = i;
					break;
				}
			}
			System.out.println("请输入图书ISBN");
			books [index][0] = sc.next();
			System.out.println("请输入书名");
			books [index][1] = sc.next();
			System.out.println("请输入价格");
			books [index][2] = sc.next();
			System.out.println("请输入出版社");
			books [index][3] = sc.next();
			System.out.println("请输入作者");
			books [index][4] = sc.next();
			
			System.out.println("增加成功！");
			bookManage();
			break;
		// 删除
		case 2:
			System.out.println("请输入ISBN：");
			String shuming = sc.next();
		    index = -1;
		    for (int i = 0; i < books.length; i++) {
				if (shuming.equals(books[i][1])) {
					index=i;
					books [index][0]=null;
					books [index][1]=null;
					books [index][2]=null;
					books [index][3]=null;
					books [index][4]=null;
					
					System.out.println("删除成功！");
					bookManage();
				}
			}
			
			
			break;
		// 更新
		case 3:
			System.out.println("请输入ISBN:");
			String bianhao = sc.next();
			index = -1;
			for (int i = 0; i < books.length; i++) {
				if (bianhao.equals(books[index][0])) {
					index=i;
					books [index][0]=sc.next();
					books [index][1]=sc.next();
					books [index][2]=sc.next();
					books [index][3]=sc.next();
					books [index][4]=sc.next();
					
					System.out.println("更新成功！");
					bookManage();
				}
			}
			break;
		// 查询
		case 4:
			System.out.println("请根据数字进行选择：1根据ISBN查询2根据书名查询（模糊）3根据出版社查询 4根据作者查询 5根据价格范围查询 6查询所有书籍信息");
			int k4=sc.nextInt(); 
			switch (k4) {
			case 1:
				//根据ISBN查询	
				isbnManage();
				break;
			case 2:
				//根据书名查询（模糊）
				shumingManage();
				break;
			case 3:
	
				break;
			case 4:
	
				break;
			case 5:
	
				break;
			case 6:
	
				break;

			default:
				break;
			}
		//返回上一级菜单	
		case 5:
			MIB();	
			break;

		default:
			break;
		}
		
	}
	private static void shumingManage() {
		System.out.println("请输入你要查询的书名：");
		String shuming = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (shuming.indexOf(shuming)!=-1) {
				System.out.print("ISBN"+"\t");
				System.out.print("书名"+"\t");
				System.out.print("价格"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.println("   ");
				System.out.println(books[i][0]+"\t"+books[i][1]+"\t"+books[i][2]+"\t"+books[i][3]+"\t"+books[i][4]);
				
				bookManage();
			}
		}
		
	}
	private static void isbnManage() {
		System.out.println("请输入ISBN：");
		String ISBN = sc.next();
		for (int i = 0; i < books.length; i++) {
			if (ISBN.equals(books[i][0])) {
				
				System.out.print("ISBN"+"\t");
				System.out.print("书名"+"\t");
				System.out.print("价格"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.println("   ");
				System.out.println(books[i][0]+"\t"+books[i][1]+"\t"+books[i][2]+"\t"+books[i][3]+"\t"+books[i][4]);
				
				bookManage();
			}
		}
		
	}
	// 初始化数据
	public static void init() {
		users[0][0] = "软件部";
		users[0][1] = "zhangsan";
		users[0][2] = "123";
		users[0][3] = "管理员";

		users[1][0] = "行政部";
		users[1][1] = "l";
		users[1][2] = "111";
		users[1][3] = "普通用户";

		books[0][0] = "10001";
		books[0][1] = "活着";
		books[0][2] = "60";
		books[0][3] = "南风出版社";
		books[0][4] = "南";

		books[1][0] = "10002";
		books[1][1] = "特殊罪案调查组";
		books[1][2] = "50";
		books[1][3] = "浙江长风出版社";
		books[1][4] = "九滴水";

		books[2][0] = "10003";
		books[2][1] = "尸语者";
		books[2][2] = "45";
		books[2][3] = "秦明出版社";
		books[2][4] = "法医秦明";

		pubs[0][0] = "南风出版社";
		pubs[0][1] = "弄口巷88号";
		pubs[0][2] = "南";

		pubs[1][0] = "浙江长风出版社";
		pubs[1][1] = "和平街53号";
		pubs[1][2] = "九滴水";

		pubs[2][0] = "秦明出版社";
		pubs[2][1] = "平安街67号";
		pubs[2][2] = "秦明";

	}

}
