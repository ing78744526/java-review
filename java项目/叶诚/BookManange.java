package ycccc;

import java.util.Scanner;

public class BookManange {
	static String loginUser = "";
    static Scanner sc = new Scanner(System.in);
    //	用户信息（部门、用户名、密码、用户角色）
    static String[][] users = new String[100][4];
    //	书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
    static String[][] books = new String[5][5];
    //	出版社信息（出版社名称、地址、联系人）
    static String[][] press = new String[5][3];
    public static void main(String[] args) {
    	initBooks();
    	initpress();
		init();
		while (true) {

            System.out.println("欢迎使用图书信息管理系统！");
            System.out.println("1.登录2.注册");

            String choice = sc.next();
            switch (choice) {
                case "1":
                    login();
                    break;
                case "2":
                	register();
                    break;
                default:
                	System.out.println("输入错误");
                    break;
            }
        }
	}
	private static void register() {
		 System.out.println("请输入用户部门：");
	        String userBranch = sc.next();
	        String userName;
	        while (true) {
	            System.out.println("请输入用户名");
	            userName = sc.next();
	            boolean flag = isUserExists(userName);
	            if (flag) {
	                System.out.println("用户名已存在，请重新输入！");
	            } else {
	                break;
	            }
	        }
	        System.out.println("请输入用户密码：");
	        String userPassword = sc.next();
	        for (int i = 0; i < users.length; i++) {
	            if (users[i][0] == null) {
	                users[i][0] = userBranch;
	                users[i][1] = userName;
	                users[i][2] = userPassword;
	                users[i][3] = "普通用户";
	                break;
	            }
	        }
	        System.out.println("注册成功:)");
	}
	private static boolean isUserExists(String userName) {
		  boolean flag = false;
	        for (int i = 0; i < users.length; i++) {
	            if (users[i][0] != null && users[i][1].equals(userName)) {
	                flag = true;
	                break;
	            }
	        }
	        return flag;
	}
	private static void login() {
		  boolean flag = false;
	        for(int j = 0;j < 4;j++) {

	            System.out.println("请输入用户名：");
	            String userName = sc.next();

	            System.out.println("请输入密码：");
	            String userPassword = sc.next();

	            for(int i = 0;i < users.length;i++) {
	                if(userName.equals(users[i][1]) && userPassword.equals(users[i][2])) {
	                    flag = true;
	                    break;
	                }
	            }

	            if(j == 2 && flag == false) {
	                System.out.println("输入错误超过3次，退出系统");
	                System.exit(0);
	            }else if(flag == true) {
	                loginUser = userName;
	                System.out.println("登陆成功！");
	                homePage();
	            }else if (flag == false && j < 3) {
	                System.out.println("该用户不存在或者密码错误！请重新登录！");
	            }
	        }
	}
	private static void homePage() {
		while (true) {
			System.out.println(loginUser+"欢迎您");
			System.out.println("1.图书管理2.出版社管理3.退出登录4.退出系统");
			String choic=sc.next();
			switch (choic) {
			case "1":
				//1.图书管理
				booksManagement();
				break;
			case "2":
				//2.出版社管理
				pressManagement();
				break;
			case "3":
				//3.退出登录
				System.out.println("已退出登录！");
				break;
			case "4":
				//4.退出系统
				System.out.println("已退出系统！");
				break;
			default:
				break;
			}
		}
		
	}
	private static void pressManagement() {
		while (true) {
			System.out.println("欢迎使用出版社管理系统");
			System.out.println("1.增加2.删除3.更新4.根据出版社名称查询5.查询所有出版社6.返回上级菜单");
			String choic=sc.next();
			switch (choic) {
			case "1":
				//1.增加
				pressIncrease();
				break;
			case "2":
				//2.删除
				pressDelete();
				break;
			case "3":
				//3.更新
				pressUpdate();
				break;
			case "4":
				//4.根据出版社名称查询
				pressInquire();
				break;
			case "5":
				//5.查询所有出版社
				AllPress();
				break;
			case "6":
				return;
			default:
				break;
			}
		}
	}
	private static void AllPress() {
		System.out.println("出版社名称\t地址\t联系人");
	        for (int i = 0; i < press.length; i++) {
	            for (int j = 0; j < press[i].length; j++) {
	                if (press[i][j] != null) {
	                    System.out.print(press[i][j] + "\t");
	                    if(j==press[i].length-1){
	                        System.out.println();
	                    }
	                }
	            }
	        }
		
	}
	private static void pressInquire() {
		String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
        for (int i = 0; i < books.length; i++) {
            if (books[i][3] != null && pressName.equals(books[i][3])) {
                System.out.println("出版社有关联的图书禁止删除！");
                return;
            }
        }
        for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && pressName.equals(press[i][0])) {
                for (int j = 0; j < press[i].length; j++) {
                    press[i][j]=null;
                }
            }
        }
        System.out.println("删除成功！");
		
	}
	private static void pressUpdate() {
		 String pressName;
	        while (true) {
	            System.out.println("请输入出版社名称：");
	            pressName = sc.next();
	            boolean exists = isPressExists(pressName);
	            if (exists) {
	                break;
	            } else {
	                System.out.println("您输入的编码不存在，请重新输入！");
	            }
	        }
	        System.out.println("请输入出版社地址：");
	        String pressAddress = sc.next();
	        System.out.println("请输入出版社联系人：");
	        String pressBoss = sc.next();

	        for (int i = 0; i < press.length; i++) {
	            if (press[i][0]!=null&& pressName.equals(press[i][0])){
	                press[i][1]=pressAddress;
	                press[i][2]=pressBoss;
	                System.out.println("修改成功！");
	                return;
	            }
	        }

	    }

	    private static void deletePressInfoByName() {
	        String pressName;
	        while (true) {
	            System.out.println("请输入出版社名称：");
	            pressName = sc.next();
	            boolean exists = isPressExists(pressName);
	            if (exists) {
	                break;
	            } else {
	                System.out.println("您输入的编码不存在，请重新输入！");
	            }
	        }
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][3] != null && pressName.equals(books[i][3])) {
	                System.out.println("出版社有关联的图书禁止删除！");
	                return;
	            }
	        }
	        for (int i = 0; i < press.length; i++) {
	            if (press[i][0] != null && pressName.equals(press[i][0])) {
	                for (int j = 0; j < press[i].length; j++) {
	                    press[i][j]=null;
	                }
	            }
	        }
	        System.out.println("删除成功！");
		
	}
	@SuppressWarnings("null")
	private static void pressDelete() {
	        while (true) {
	            System.out.println("请输入出版社名称：");
	            String pressName = sc.next();
	            boolean exists = isPressExists(pressName);
	            if (exists) {
	                break;
	            } else {
	                System.out.println("您输入的编码不存在，请重新输入！");
	            }
	        }
	        Object pressName = null;
			for (int i = 0; i < books.length; i++) {
	            if (books[i][3] != null && pressName.equals(books[i][3])) {
	                System.out.println("出版社有关联的图书禁止删除！");
	                return;
	            }
	        }
	        for (int i = 0; i < press.length; i++) {
	            if (press[i][0] != null && pressName.equals(press[i][0])) {
	                for (int j = 0; j < press[i].length; j++) {
	                    press[i][j]=null;
	                }
	            }
	        }
	        System.out.println("删除成功！");
	    }

	    private static void getAllPressInfo() {
	        System.out.println("出版社名称\t地址\t联系人");
	        for (int i = 0; i < press.length; i++) {
	            for (int j = 0; j < press[i].length; j++) {
	                if (press[i][j] != null) {
	                    System.out.print(press[i][j] + "\t");
	                    if(j==press[i].length-1){
	                        System.out.println();
	                    }
	                }
	            }
	        }
		
	}
	private static void pressIncrease() {
		while (true) {
            System.out.println("请输入出版社名称：");
            String pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                System.out.println("您输入的编码已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入出版社地址：");
        String pressAddress = sc.next();
        System.out.println("请输入出版社联系人：");
        String pressBoss = sc.next();
        for (int i = 0; i < press.length; i++) {
            if (press[i][0]==null){
                String pressName = null;
				press[i][0]=pressName;
                press[i][1]=pressAddress;
                press[i][2]=pressBoss;
                System.out.println("添加成功！");
                return;
            }
        }
        System.out.println("添加失败");
		
	}
	private static boolean isPressExists(String pressName) {
		boolean flag = false;
        for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && press[i][0].equals(pressName)) {
                flag = true;
                break;
            }
        }
        return flag;
	}
	private static void booksManagement() {
		while (true) {
			System.out.println("欢迎使用图书管理系统");
			System.out.println("1.增加2.删除3.更新4.查询5.返回上级菜单");
			String choic=sc.next();
			switch (choic) {
			case "1":
				//1.增加
				bookIncrease();
				break;
			case "2":
				//2.删除
				bookDelete();
				break;
			case "3":
				//3.更新
				bookUpdate();
				break;
			case "4":
				//4.查询
				bookInquire();
				break;
			case "5":
				//5.返回上级菜单
				return;
			default:
				break;
			}
		}
		
	}
	private static void bookInquire() {
		 while (true) {
	            System.out.println("1.根据ISBN查询\t2.根据书名查询（模糊）\t3.根据出版社查询\t4.根据作者查询\t5.根据价格范围查询\t6.查询所有书籍信息\t7.返回上一级菜单");
	            int choice = sc.nextInt();
	            switch (choice) {
	                case 1:   
	                	//1.根据ISBN查询
	                    getBookISBN();
	                    break;
	                case 2:
	                	//.根据书名查询
	                    getBookName();
	                    break;
	                case 3:
	                	//根据出版社查询
	                    getBookPress();
	                    break;
	                case 4:
	                	//根据作者查询
	                    getBookWriterName();
	                    break;
	                case 5:
	                	//根据价格
	                   getBookPrice();
	                    break;
	                case 6:
	                	//查询所有
	                   getAllBook();
	                    break;
	                case 7:
	                    return;
	                default:
	                    System.out.println("输入错误，请重新输入！");
	                    break;
	            }
	        }
		
	}
	private static void getBookPrice() {
		 double min;
	        double max;
	        lo:
	        while (true) {
	            System.out.println("请输入最低价");
	            min = sc.nextDouble();
	            System.out.println("请输入最高价");
	            max = sc.nextDouble();
	            for (int i = 0; i < books.length; i++) {
	                if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
	                    break lo;
	                }
	            }
	            System.out.println("未查询到相关信息，请重新输入");
	        }
	        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
	                for (int j = 0; j < books[i].length; j++) {
	                    System.out.print(books[i][j] + "\t");
	                }
	                System.out.println();
	            }
	        }
		
	}
	private static void getAllBook() {
		System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books.length; j++) {
                if (books[i][j] != null) {
                    System.out.print(books[i][j] + "\t");
                    if(j==books.length-1){
                        System.out.println();
                    }
                }
            }
        }
		
	}
	private static void getBookWriterName() {
		String bookName;
        lo:
        while (true) {
            System.out.println("请输入图书名字包含的内容");
            bookName = sc.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
	}
	private static void getBookPress() {
		 String name;
	        lo:
	        while (true) {
	            System.out.println("请输入图书出版社名");
	            name = sc.next();
	            for (int i = 0; i < books.length; i++) {
	                if (books[i][3] != null && name.equals(books[i][3])) {
	                    break lo;
	                }
	            }
	            System.out.println("未查询到相关信息，请重新输入");
	        }

	        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][3] != null && name.equals(books[i][3])) {
	                for (int j = 0; j < books[i].length; j++) {
	                    System.out.print(books[i][j] + "\t");
	                }
	                System.out.println();
	            }
	        }
		
	}
	private static void getBookName() {
	        while (true) {
	            System.out.println("请输入图书名字包含的内容");
	           String bookName = sc.next();
	            for (int i = 0; i < books.length; i++) {
	                if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
	                    break;
	                }
	            }
	            System.out.println("未查询到相关信息，请重新输入");
	        }
		
	}
	private static void getBookISBN() {
	        System.out.println("请输入图书编号（ISBN）");
	        String num = sc.next();
	        int index = -1;
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][0] != null && num.equals(books[i][0])) {
	                index = i;
	                break;
	            }
	        }
	        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
	        for (int i = 0; i < books[index].length; i++) {
	            System.out.print(books[index][i] + "\t");
	        }
	        System.out.println();
	}
	@SuppressWarnings("null")
	private static void bookUpdate() {
		 while (true) {
	            System.out.println("请输入您想更新的图书编码（ISBN）");
	            String bookNum = sc.next();
	            boolean isExists = isBookExists(bookNum);
	            if (isExists) {
	                break;
	            } else {
	                System.out.println("查无信息，请重新输入！");
	            }
	        }
	        for (int i = 0; i < books.length; i++) {
	            Object bookNum = null;
				if (books[i][0] != null && bookNum.equals(books[i][0])) {
	                for (int j = 0; j < books[i].length; j++) {
	                    books[i][j] = null;
	                }
	            }
	        }
	        System.out.println("更新成功！");
		
	}
	private static void bookDelete() {
		 while (true) {
	            System.out.println("请输入您想删除的图书编码（ISBN）");
	            String bookNum = sc.next();
	            boolean isExists = isBookExists(bookNum);
	            if (isExists) {
	                break;
	            } else {
	                System.out.println("查无信息，请重新输入！");
	            }
	        }
		 System.out.println("请输入书籍名称：");
	        String bookName = sc.next();
	        System.out.println("请输入书籍价格：");
	        String bookPrice = sc.next();
	        System.out.println("请输入书籍出版社：");
	        String bookPress = sc.next();
	        System.out.println("请输入书籍作者：");
	        String bookWriter = sc.next();
	        int index=-1;
	        for (int i = 0; i < books.length; i++) {
				Object bookNum = null;
				if (books[i][0]!=null && books[i][0].equals(bookNum)) {
					index=i;
					break;
				}
				
			}
		
	}
	private static void bookIncrease() {
		 String bookNum;
	        while (true) {
	            System.out.println("请输入编码（ISBN）：");
	            bookNum = sc.next();
	            boolean exists = isBookExists(bookNum);
	            if (exists) {
	                System.out.println("您输入的编码已存在，请重新输入！");
	            } else {
	                break;
	            }
	        }
	        System.out.println("请输入书籍名称：");
	        String bookName = sc.next();
	        System.out.println("请输入书籍价格：");
	        String bookPrice = sc.next();
	        System.out.println("请输入书籍出版社：");
	        String bookPress = sc.next();
	        System.out.println("请输入书籍作者：");
	        String bookWriter = sc.next();
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][0] == null) {
	                books[i][0] = bookNum;
	                books[i][1] = bookName;
	                books[i][2] = bookPrice;
	                books[i][3] = bookPress;
	                books[i][4] = bookWriter;
	                System.out.println("添加书籍成功！");
	                return;
	            }
	        }
	        System.out.println("添加失败");
	    }
	private static boolean isBookExists(String bookNum) {
		 boolean flag = false;
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][0] != null && books[i][0].equals(bookNum)) {
	                flag = true;
	                break;
	            }
	        }
	        return flag;
	}
	private static void init() {
		users[0][0]="1"; 
		users[0][1]="admin";
		users[0][2]="1111";
		users[0][3]="管理员";
	}
	private static void initpress() {
		press[0][0]="人民文学出版社";
		press[0][1]="北京市 XXX";
		press[0][2]="XX";
	}
	private static void initBooks() {
		books[0][0]="7894546";
		books[0][1]="《哈利波特》";
		books[0][2]="60.0";
		books[0][3]="人民文学出版社";
		books[0][4]="J.k罗琳";
		books[1][0]="7794546";
		books[1][1]="《孙子兵法》";
		books[1][2]="50.0";
		books[1][3]="人民文学出版社";
		books[1][4]="孙子";
		
	}
}
