package ss;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
	static String userInfo[][] = new String[4][5];
	static String libraryArray[][] = new String[5][5];
	static String newArray[][] = null;
	static Scanner scan = new Scanner(System.in);
	public static final String MASTER = "admin";
	static int count = 3;
	static int u = 0;
	static int newOne = 0;
	
	public static void main (String[] args) {
		init();
		welcome();
	}
	
	public static void init() {
		// name
		userInfo[0][0] = "张三";
		// userName
		userInfo[0][1] = "zhangsan";
		// password
		userInfo[0][2] = "123";
		// id
		userInfo[0][3] = "007";
		// position
		userInfo[0][4] = "管理员";
		
		userInfo[1][0] = "李四";
		userInfo[1][1] = "admin";
		userInfo[1][2] = "123";
		userInfo[1][3] = "008";
		userInfo[0][4] = "Super[管理员]";
		
		libraryArray[0][0] = "007";
		libraryArray[0][1] = "你好";
		libraryArray[0][2] = "新型出版社";
		
		libraryArray[1][0] = "008";
		libraryArray[1][1] = "再见";
		libraryArray[1][2] = "大型出版社";
		
	}
	
	public static void welcome() {
		System.out.println("欢迎来到登录页面：");
		System.out.println("请输入1:登录,2:注册,或者按任意数字键退出");
		int i = checkNumber();
		login(i);
	}
	
	public static int checkNumber() {
		String str = scan.next();
		while(true) {
			if(str.replaceAll("[0-9]","").length()!=0) {
				System.out.println("输入错误，请重新尝试");
				System.out.println("请输入1:登录,2:注册,或者按任意数字键退出");
				str = scan.next();
			}else {
				break;
			}
		}
		int i = Integer.parseInt(str);
		return i;
	}
	
	public static void login(int key) {
		switch (key) {
		case 1:
			loginInfo();
			break;
		case 2:
			registerInfo();
			break;
		default:
			Turn(count);
			System.out.println("您已成功退出！");
			System.exit(0);
		}
	}

	private static void loginInfo() {
		for (int j = 0; j < 3; j++) {
			System.out.println("请输入用户名：");
			String userName = scan.next();
			System.out.println("请输入密码：");
			String password = scan.next();
			if (userName != null || userName.length() != 0) {
				boolean flag = check(userName, password);
				if(flag) {
					System.out.println("登录成功!");
					homePage(userName);
					return;
				} else {
					System.out.println("登录失败!");
				}
			}
		}
		System.out.println("您输入错误的次数过多，系统已自动退出");
		Turn(count);
		System.exit(0);
	}

	private static void registerInfo() {
		System.out.println("请输入昵称：");
		String nickName = scan.next();
		System.out.println("请输入用户名：");
		String userName = scan.next();
		System.out.println("请输入密码：");
		String password = scan.next();
		if (userName != null || userName.trim().length() != 0) {
			int i = insert(nickName, userName, password);
			if(i != -1) {
				userInfo[i][0] = nickName;
				userInfo[i][1] = userName;
				userInfo[i][2] = password;
				userInfo[i][4] = "普通用户";
			}
			System.out.println("恭喜你注册成功!");
			loginInfo();
		}
		
	}
	
	private static int insert(String nickName, String userName, String password) {
		int index = -1;
		for (int i = 0; i < userInfo.length; i++) {
			if (userInfo[i][0] == null) {
				index = i;
				break;
			}
		}
		if(index == -1) {
			int u = userInfo.length + 50;
	        String[][] newUsers = new String[u][4];
	
	        for (int i = 0; i < userInfo.length; i++) {
	            for (int j = 0; j < userInfo[i].length; j++) {
	                newUsers[i][j] = userInfo[i][j];
	            }
	        }
	        newUsers[userInfo.length][0] = nickName;
	        newUsers[userInfo.length][1] = userName;
	        newUsers[userInfo.length][2] = password;
	        newUsers[userInfo.length][4] = "普通用户";
	        userInfo = newUsers;
		}
		return index;
		
	}
	
	private static boolean check(String userName, String password) {
		boolean flag = false;
		for (int i = 0; i < userInfo.length; i++) {
			if (userName.equals(userInfo[i][1]) && password.equals(userInfo[i][2])) {
				flag = true;
			}
		}
		return flag;		
	}
	
	private static String[][] arrayCheck() {
		if(newOne == 0) {
			return userInfo;
		} else {
			return newArray;
		}
	}

	private static void homePage(String userName) {
		System.out.println("欢迎来到图书系统！");
		if (userName.equals(MASTER)) System.out.println("您为超级管理员，拥有最高权限");
		System.out.println("您可以使用以下功能：1、增加书籍 2.删除书籍 3.更新书籍信息 4.查询书籍信息 5.返回登陆界面（按其他键退出）");
		int key = checkNumber();
		switch (key) {
		case 1:
			insertBook(userName);
			break;
		case 2:
			deleteBook(userName, key);
			break;
		case 3:
			deleteBook(userName, key);
			break;
		case 4:
			selectBook(userName);
			break;
		case 5: 
			welcome();
			break;
		default:
			Turn(count);
			System.out.println("系统安全退出，感谢您的使用。");
			System.exit(0);
			break;
		
		}
	     
	}
	
	private static void selectBook(String userName) {
		System.out.println("1.INFO信息查询 2.根据书名查询 3.出版社信息查询 4.查询所有出版社 (按任意键回到主页)");
		int key = checkNumber();
		switch (key) {
		case 1:
			selectInfo(userName);
			break;
		case 2:
			selectName(userName);
			break;
		case 3:
			selectPublish(userName);
			break;
		case 4:
			selectAllPublish(userName);
			break;
		default:
			homePage(userName);
			break;
		}
		
	}
		
	private static void selectPublish(String userName) {
		System.out.print("请输入要查询的出版社名称:");
		int count = 0;
		for (int i = 0; i < libraryArray.length; i++) {
			if (libraryArray[i][2] != null) {
				System.out.print((i + 1) + "、" + libraryArray[i][2] + " ");
				count++;
			}
			
		}
		System.out.println();
		int i = scan.nextInt();
		while(true) {
			int count2 = 0;
			if(i > count || i < 1) {
				System.out.println("您输入的范围有误，请重新输入");
				i = scan.nextInt();
			}else {
				break;
			}
			count2++;
			if(count2 == 3) {
				System.out.println("输入错误次数过多，系统重新登陆.");
				welcome();
			}
		}
		count = 0;
		for (int j = 0; j < libraryArray.length; j++) {
			if (libraryArray[j][2] != null) {
				count++;
				if(count == i) {
					System.out.println(libraryArray[count][0] + " \t " + libraryArray[count][1] + " \t " + libraryArray[count][2] + " \t " + libraryArray[count][3]);
					break;
				}
			}
		}
		Turn(count);
		System.out.println("跳转成功！");
		homePage(userName);
		
		
	}

	private static void selectInfo(String str) {

		System.out.println("请输入要输入的编号：");
		
		String info = scan.next();
		
		int index = -1;
		for (int i = 0; i < libraryArray.length; i++) {
			if (info.equals(libraryArray[i][0])) {
				index = i;
			}
		}
		
		if (index != -1) {
			System.out.println("书籍编号 \t 书籍名称 \t 出版社名称 \t 作者名称");
			System.out.println(libraryArray[index][0] + " \t " + libraryArray[index][1] + " \t " + libraryArray[index][2] + " \t " + libraryArray[index][3]);
			Turn(count);
			System.out.println("跳转成功");
			homePage(str);
		} else {
			System.out.println("数据错误，请重试。");
			u++;
			error(u);
			selectInfo(str);
		}
		
	}
	
	private static void selectName(String name) {
		System.out.println("请输入要搜索的书籍名称");
		String info = scan.next();
		
		int index = -1;
		for (int i = 0; i < libraryArray.length; i++) {
			if(libraryArray[i][1] != null && libraryArray[i][1].indexOf(info) != -1) {
				index = i;
			}
		}
		
		if (index != -1) {
			System.out.println("书籍编号 \t 书籍名称 \t 出版社名称 \t 作者名称");
			System.out.println(libraryArray[index][0] + " \t " + libraryArray[index][1] + " \t " + libraryArray[index][2] + " \t " + libraryArray[index][3]);
			Turn(count);
			System.out.println("跳转成功");
			homePage(name);
		} else {
			System.out.println("数据错误，请重试。");
			u++;
			error(u);
			selectName(name);
		}
	}
	
	private static void selectAllPublish(String userName) {
		for (int i = 0; i < libraryArray.length; i++) {
			if (libraryArray[i][2] != null) {
				System.out.print(libraryArray[i][2] + " ");
			}
		}
		System.out.println();
		Turn(count);
		System.out.println("跳转成功");
		homePage(userName);
	}

	public static void error (int u) {
		if(u == 3) {
			System.out.println("数据错误次数过多，请重新登陆。");
			u = 0;
			Turn(count);
			welcome();
		}
	}
	

	private static void deleteBook(String userName, int key) {
		String name = null; 
		if(key == 2) {
			name = "删除";
		}else{
			name = "更新";
		}
		System.out.println("请输入要" + name + "的书籍名称：");
		String bookName = scan.next();
		if(!userName.equals(MASTER)) {
			System.out.println("您不是管理员，没有" + name + "权限");
		} else {
			int index = -1;
			for (int i = 0; i < libraryArray.length; i++) {
				if (bookName.equals(libraryArray[i][1])) {
					index = i;
					break;
				}
			}
			if(index != -1 && key == 2) {
				libraryArray[index][0] = null;
				libraryArray[index][1] = null;
				libraryArray[index][2] = null;
				libraryArray[index][3] = null;
				System.out.println(name + "成功，现已跳转至主界面");
				Turn(count);
				System.out.println("跳转成功");
				homePage(userName);
			} else if(index != -1 && key != 2){
				System.out.println("请输入要修改的书籍编号：");
				libraryArray[index][0] = scan.next();
				System.out.println("请输入要修改的书籍名称：");
				libraryArray[index][1] = scan.next();
				System.out.println("请输入修改的出版社名称：");
				libraryArray[index][2] = scan.next();
				System.out.println("请输入修改的作者名称：");
				libraryArray[index][3] = scan.next();
				System.out.println(name + "成功，现已跳转至主界面");
				Turn(count);
				System.out.println("跳转成功");
				homePage(userName);
			} else {
				System.out.println("您输入的书籍信息有误，请重试。");
				u++;
				error(u);
				deleteBook("admin", key);
			}
		}
		
		
	}

	private static void insertBook(String name) {
		int index = -1;
		for (int i = 0; i < libraryArray.length; i++) {
			if (libraryArray[i][0] == null) {
				index = i;
				break;
			}
		}
		if(index == -1) {
			System.out.println("暂时不支持添加书籍，请稍后再试。");
			homePage(name);
		}
		System.out.println("请输入要加入的书籍编号：");
		libraryArray[index][0] = scan.next();
		System.out.println("请输入要加入的书籍名称：");
		libraryArray[index][1] = scan.next();
		System.out.println("请输入书籍的出版社名称：");
		libraryArray[index][2] = scan.next();
		System.out.println("请输入书籍的作者名称：");
		libraryArray[index][3] = scan.next();
		
		System.out.println("您已添加成功，现已跳往主界面:");
		Turn(count);
		System.out.println("您已成功跳转");
		homePage(name);
	}
	
	public static void Turn(int count) {
		for (int i = 0; i < count; i++) {
			System.out.println("倒计时"+count+"秒");
			TurnException();
			count--;
		}	
	}
	
	public static void TurnException() {
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	public static int reg (String regex) {
		int index = -1;
		for(int i = 0; i < libraryArray.length; i++) { 
	    	 Pattern pattern = Pattern.compile(regex);
		     Matcher matcher = pattern.matcher(libraryArray[i][1]);
		     if(matcher.matches() == true) index = i;
	     }
		return index;
	}


}