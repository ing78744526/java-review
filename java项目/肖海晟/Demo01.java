package De.Mo;

import java.util.Scanner;

public class Demo01 {
	static Scanner scanner = new Scanner(System.in);
	static String user[][] = new String [20][4];
	static String books[][] = new String [20][5];
	static String press[][] = new String [20][3];
	static String userName = " ";
	public static void main(String[] args) {
		user();
		books();
		press();
		index();
	}
	public static void index() {
		System.out.println("图书管理系统");
		System.out.println("1.登录 2.注册 3.退出系统");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			loginView();
			break;
		case 2:
			register();
			break;
		case 3:
			System.exit(0);
		default:
			break;
		}
	}
	public static void loginView () {
		System.out.println("请输入用户名：");
		String userName = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();
		
		boolean log = login(userName,password);
		if (log) {
			System.out.println("登录 成功");
			System.out.println(userName+"， 欢迎您使用闽大书籍管理系统！！！");
			manage();
		}else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
			loginView();
		}
	}
	public static boolean login(String userName , String password) {
		boolean log = false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1])&&password.equals(user[i][2])) {
				log = true ;
			}
		}
		return log ;
	}
	public static void register() {
		int index = -1;
		for (int i = 0; i < user.length; i++) {
			if (user[i][0]==null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入所属部门：");
		user[index][0] = scanner.next();
		System.out.println("请输入用户名：");
		user[index][1] = scanner.next();
		System.out.println("请输入密码：");
		user[index][2] = scanner.next();
		System.out.println("请输入角色：");
		user[index][3] = scanner.next();
		System.out.println("注册成功");
		loginView();
	} 
	public static void manage() {
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int man = scanner.nextInt();
		switch (man) {
		case 1:
			administration();
			break;
		case 2:
			publishing();
			break;
		case 3:
			System.out.println("退出登录成功！");
			index();
			break;
		case 4:
			System.out.println("系统退出成功！");
			System.exit(0);
			break;

		default:
			break;
		}
	}
	public static void administration() {
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int adm = scanner.nextInt();
			switch (adm) {
			case 1:
				int index = -1;
				for (int i = 0; i < user.length; i++) {
					if (user[i][0]==null) {
						index = i;
						break;
					}
				}
				System.out.println("请输入图书ISBN:");
				books[index][0] = scanner.next();
				System.out.println("请输入书名:");
				books[index][1] = scanner.next();
				System.out.println("请输入价格:");
				books[index][2] = scanner.next();
				System.out.println("请输入出版社:");
				books[index][3] = scanner.next();
				System.out.println("请输入作者:");
				books[index][4] = scanner.next();
				System.out.println("添加成功");
				break;
			case 2:
				System.out.println("请输入要删除书籍的名称：");
				String booksName = scanner.next();
				boolean flag = false;
				for (int i = 0; i < books.length; i++) {
					if (booksName.equals(books[i][1])) {
						flag = true;
						books[i][0] = null;
						books[i][1] = null;
						books[i][2] = null;
						books[i][3] = null;
						books[i][4] = null;
					}
				}
				if (flag) {
					System.out.println("删除成功");
				}else {
					System.out.println("没有找到该书! \r\n"+"删除失败");
				}
				break;
			case 3:
				System.out.println("请输入要更新书籍的ISBN：");
				String bookschange = scanner.next();
				boolean fal = false;
				for (int i = 0; i < books.length; i++) {
					if (bookschange.equals(books[i][0])) {
						System.out.println("请输入图书ISBN:");
						books[i][0] = scanner.next();
						System.out.println("请输入书名:");
						books[i][1] = scanner.next();
						System.out.println("请输入价格:");
						books[i][2] = scanner.next();
						System.out.println("请输入出版社:");
						books[i][3] = scanner.next();
						System.out.println("请输入作者:");
						books[i][4] = scanner.next();
					}
				}
				if (fal) {
					System.out.println("更新完成");
				}else {
					System.out.println("该ISBN号不存在！！！");
				}
				break;
			case 4:
				
				break;
			case 5:
				manage();
				break;
			default:
				break;
		}
	}
	}
	public static void publishing() {
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int pub = scanner.nextInt();
			switch (pub) {
			case 1:
				int index = -1;
				for (int i = 0; i < user.length; i++) {
					if (user[i][0]==null) {
						index = i;
						break;
					}
				}
				System.out.println("请输入出版社名称：");
				books[index][0] = scanner.next();
				System.out.println("请输入出版社地址：");
				books[index][1] = scanner.next();
				System.out.println("请输入出版社联系人：");
				books[index][2] = scanner.next();
				System.out.println("添加成功");
				break;
			case 2:
				System.out.println("请输入要删除出版社的名称：");
				String booksName = scanner.next();
				boolean flag = false;
				for (int i = 0; i < press.length; i++) {
					if (booksName.equals(press[i][1])) {
						flag = true;
						books[i][0] = null;
						books[i][1] = null;
						books[i][2] = null;
					}
				}
				if (flag) {
					System.out.println("删除成功");
				}else {
					System.out.println("没有找到该出版社! \r\n"+"删除失败");
				}
				break;
			case 3:
				System.out.println("请输入要更新的出版社名称：");
				String bookschange = scanner.next();
				boolean fal = false;
				for (int i = 0; i < press.length; i++) {
					if (bookschange.equals(press[i][0])) {
						System.out.println("请输入出版社名称：");
						press[i][0] = scanner.next();
						System.out.println("请输入出版社地址：");
						press[i][1] = scanner.next();
						System.out.println("请输入出版社联系人：");
						press[i][2] = scanner.next();
					}
				}
				if (fal) {
					System.out.println("更新完成");
				}else {
					System.out.println("该出版社不存在！！！");
				}
				break;
			case 4:
				
				break;
			case 5:
				
				break;
			case 6:
				manage();
				break;
			default:
				break;
			}
		}
	}
	public static void user() {
		user[0][0] = "技术部";
		user[0][1] = "123";
		user[0][2] = "123";
		user[0][3] = "员工";
		user[1][0] = "宣传部";
		user[1][1] = "456";
		user[1][2] = "456";
		user[1][3] = "管理员";
	}
	public static void books() {
		books[0][0] = "97871010";
		books[0][1] = "史记";
		books[0][2] = "125.0";
		books[0][3] = "中华书局";
		books[0][4] = "司马迁";
		books[1][0] = "97875063";
		books[1][1] = "活着";
		books[1][2] = "20.0";
		books[1][3] = "作家出版社";
		books[1][4] = "余华";
		books[2][0] = "97872290";
		books[2][1] = "三体全集";
		books[2][2] = "168.0";
		books[2][3] = "重庆出版社";
		books[2][4] = "刘慈欣";
		books[3][0] = "97875442";
		books[3][1] = "白夜行";
		books[3][2] = "39.5";
		books[3][3] = "南海出版公司";
		books[3][4] = "[日]东野圭吾";
	}
	public static void press() {
		press[0][0] = "中华书局";
		press[0][1] = "北京市王府井大街36号";
		press[0][2] = "张三";
		press[1][0] = "作家出版社";
		press[1][1] = " 香港九龙荷李活商业中心8楼";
		press[1][2] = "李四";
		press[2][0] = "重庆出版社";
		press[2][1] = "茶园新区开拓路6号";
		press[2][2] = "王五";
		press[3][0] = "南海出版公司";
		press[3][1] = "海秀中路51-1号星华大厦5楼";
		press[3][2] = "周毅";
	}
}