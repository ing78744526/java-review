import java.util.Arrays;
import java.util.Scanner;

public class Demo0001 {
	//用户信息（部门、用户名、密码、用户角色）
	//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
	//出版社信息（出版社名称、地址、联系人）
	static String [][]user = new String [20][4];
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		init();
		
		System.out.println("欢迎来到闽大图书管理系统！！！");
		System.out.println("1.登录  ; 2.注册  ");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			login();
			break;

		case 2:
			register();
			break;
		}
	}

	private static void register() {
		System.out.println("请输入部门:");
		String departName = scanner.next();
		System.out.println("请输入用户名:");
		String userName = scanner.next();
		System.out.println("请输入密码:");
		String password = scanner.next();
		for (int i = 0; i < user.length; i++) {
			if (user[i][0] ==null) {
				user[i][0]=departName;
				user[i][1]=userName;
				user[i][2]=password;
				user[i][3]="普通用户";
			}
		}
		System.out.println("注册成功！");
		System.out.println("欢迎来到闽大图书管理系统！");
		System.out.println("1.登录  ; 2.注册  ");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			login();
			break;

		case 2:
			register();
			break;
		}
	}


	private static void login() {
		for (int i = 0; i < 3; i++) {
			System.out.println("请输入用户名:");
			String userName = scanner.next();
			System.out.println("请输入密码:");
			String password = scanner.next();
			boolean flag = login(userName,password);
			if (flag) {
				System.out.println("登陆成功！");
				homepage(userName);
				break;
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
				}
		 if (i==2) {
			 System.out.println("输入错误超过3次,系统自动退出");
		}
		}
		}
		

	private static void homepage(String userName) {
		System.out.println(userName+",欢迎您使用闽大书籍管理系统！");
		
	}

	private static boolean login(String userName, String password) {
		Boolean flag = false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1])&&password.equals(user[i][2])) {
				flag = true;
			}
		}
		return flag;
	}

	private static void init() {
		user [0][0] = "A部" ;
		user [0][1] = "yyds";
		user [0][2] = "wo20";
		user [0][3] = "管理员";
		
		user [1][0] = "B部" ;
		user [1][1] = "cpdd";
		user [1][2] = "wo880";
		user [1][3] = "普通用户";
		System.out.println(Arrays.toString(user[2]));
	}
}
