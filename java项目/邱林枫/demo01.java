

import java.util.Iterator;
import java.util.Scanner;

public class demo01 {

	static String[][] user = new String[100][4];
	static String[][] book = new String[100][5];
	static String[][] pub = new String[100][3];
	static Scanner sc = new Scanner(System.in);
	static String loginuser = " ";

	public static void main(String[] args) {
		users();
		books();
		pub();
		indexpage();
	}

	private static void indexpage() {
		
		System.out.println("     "+"图书管理系统");
		System.out.println("1.登录" + "\t" + "2.注册");

		int key = sc.nextInt();
		switch (key) {
		case 1:
			login();
			break;
		case 2:
			registr();
			break;
		default:
			System.out.println("系统退出！");
			System.exit(0);
			break;
		}
	}
	
	private static boolean login() {
		boolean logFlag = false;
		while (!logFlag) {
			System.out.println("用户名");
			String userName = sc.next();
			System.out.println("密码");
			String password = sc.next();
//			判断对错方法1 新建一个方法判断
			boolean result = loginCheck(userName,password);
			if (result) {
				System.out.println("登录成功");
				homepage();
				logFlag = true;
			}else {
				System.out.println("登录失败");
			}
		}
		return false;
	}

	private static boolean loginCheck(String userName, String password) {
		boolean logFlag = false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1]) && password.equals(user[i][2])) {
				logFlag=true;
				break;
			}
		}
		return logFlag;
	}

	private static void registr() {

		while (true) {
			int index = -1;
			for (int i = 0; i < user.length; i++) {
				if (user[i][1] == null) {
					index = i ;
					break;
				}
			}
		
			System.out.println("请输入部门");
			user[index][0] = sc.next();
			System.out.println("请输入用户名");
			user[index][1] = sc.next();
			System.out.println("请输入密码");
			user[index][2] = sc.next();
			System.out.println("确认密码");
			user[index][3] = sc.next();
			
			if (user[index][2].equals(user[index][3])) {
				System.out.println("注册成功");
				login();
				break;
			} else {
				System.out.println("两次密码不一致，请重新登录");
			}
	
		}
	}
	
	private static void homepage() {
		System.out.println("欢迎 " + loginuser + "欢迎来到图书管理系统");
		while (true) {
			System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统\r\n");
			int key = sc.nextInt();
			switch (key) {
			case 1:
				bookmanage();
				break;
			case 2:
				pubmanage();
				break;
			case 3:
				loginuser = " ";
				indexpage();
				break;
			case 4:
				System.out.println("系统退出！");
				System.exit(0);
				break;
			default:
				break;
			}
		}
	}

	//	出版社系统   1.增 add  2.删 delete 3.改 update  4.查    check
	private static void bookmanage() {
		
		while(true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int key = sc.nextInt();
		
			switch (key) {
			case 1:
				add();
				break;
			case 2:
				delete();
				break;
			case 3:
				update();
				break;
			case 4:
				select();
				break;
			case 5:
				homepage();
				break;
			default:
				break;
			}
		}	
	}
	
//	1.增 add
	private static void add() {
		int index = -1 ;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入图书ISBN:");
		book[index][0] = sc.next();
		while(true) {
			System.out.println("请输入书名:");
			String bookName = sc.next();
			boolean result = booknamecheck(bookName);
			if (result) {
				System.out.println("添加失败！！！该书已经存在！！！");
			}else {	
				book[index][1] = bookName;
				System.out.println("请输入价格:");
				book[index][2] = sc.next();
				System.out.println("请输入出版社:");
				book[index][3] = sc.next();
				System.out.println("请输入作者:");
				book[index][4] = sc.next();	
				System.out.println("添加成功！！！");
				break;
			}
		}
	
	}
	
//	2.删 delete
//	判断对错方法2 在for循环中的情况，也可以用新建一个方法来判断
	private static void delete() {
		System.out.println("请输入要删除的书本名称：");
		String bookname = sc.next();
		int index = -1 ;
		boolean result = booknamecheck(bookname);
		for (int i = 0; i < book.length; i++) {
			if (result) {
				index = i;
				book[index][0] = null;
				book[index][1] = null;
				book[index][2] = null;
				book[index][3] = null;
				book[index][4] = null;
				
				System.out.println("删除成功！！！");
				break;
			}else {
				System.out.println("没有找到该书!");
				System.out.println("删除失败");
				break;
			}
		}	
	}
	
// 书名判断
	private static boolean booknamecheck(String bookname) {
		boolean Flag = false;
		for (int i = 0; i < book.length; i++) {
			if (bookname.equals(book[i][1])) {
				Flag = true;
				break;
			}
		}
		return Flag;
	}
	
//	3.改 update 
	private static void update() {
		System.out.println("请输入ISBN号：");
		String ISBN = sc.next();
		int index = -1 ;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0].equals(ISBN)) {
				index = i;
				break;
			}
		}
		System.out.println("请输入新的书名:");
		book[index][1] = sc.next();
		System.out.println("请定义新的价格:");
		book[index][2] = sc.next();
		System.out.println("请输入新的出版社:");
		book[index][3] = sc.next();
		System.out.println("请输入新的作者:");
		book[index][4] = sc.next();
		System.out.println("更新成功！！！");
	}
	
//	4.查   check  （1.ISBN 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有）
	private static void select() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社  4.作者  5.价格范围   6.查询所有   7.返回上一级");
		int key2 = sc.nextInt();
		switch (key2) {
		case 1:
			isbnselect();
			break;
		case 2:
			nameselect();
			break;
		case 3:
			press();
			break;
		case 4:
			author();
			break;
		case 5:
			price();
			break;
		case 6:
			all();
			break;
		case 7:
			bookmanage();
			break;


		default:
			break;
		}
		
	}
	
//	1.ISBN 
//	判断对错方法3 可以直接定义一个布尔类型，在for循环内，如果没进去if的话，Flag还是true值，就代表没有符合if条件的值；
	private static void isbnselect() {
		
		boolean Flag = true;
		System.out.println("请输入ISBN号：");
		String ISBN = sc.next();
		
		for (int i = 0; i < book.length; i++) {
			if (ISBN.equals(book[i][0])) {
				System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
				select();
				Flag=false;
				break;
			}
		}
		if (Flag) {
			System.out.println("该书不存在");
		}
		select();
	}
	
//	2.书名（模糊）
	private static void nameselect() {
		System.out.println("请输入书名：");
		String str = sc.next();
		
		for (int i = 0; i < book.length; i++) {
				if (str.indexOf(book[i][1])!= -1) {
					System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
					System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
					System.out.println();
					select();
				}	
		}
	}

//	3.出版社
	private static void press() {
		System.out.println("请输入出版社前的数字进行选择：");
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!= null) {
				System.out.print((i+1)+"."+book[i][3]+"\t");
			}
		}
		int key = sc.nextInt();
		switch (key) {
		case 1:
			String press1 = book[key-1][3];
			pub1(press1);
			break;
		case 2:
			String press2= book[key-1][3];
			pub1(press2);
			break;
		case 3:
			String press3 = book[key-1][3];
			pub1(press3);
			break;
		case 4:
			String press4 = book[key-1][3];
			pub1(press4);
			break;
			
		default:
			break;
		}
		
	}
	
	private static void pub1(String a) {
		
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!= null && book[i][3] == a ) {
				System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
				select();
				break;
			}
		}
	}

//	 4.作者
	private static void author() {
		System.out.println("请输入作者姓名：");
		String str = sc.next();
		
		for (int i = 0; i < book.length; i++) {
			if (book[i][4]!= null ) {
				if (book[i][4].equals(str)) {
					System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
					System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
					System.out.println();
					select();
				}
			}else {
				System.out.println("找不到该作者");
				select();
				break;
			}
		}
	}

//	5.价格范围 
	private static void price() {
		System.out.println("请输入最低价格");
		int min = sc.nextInt();
		System.out.println("请输入最高价格");
		int max = sc.nextInt();
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!= null) {
				int num = Integer.parseInt(book[i][2]);
				if (num >= min && num <= max) {
					System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
					System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
					System.out.println();
				}
			}
		}
	}
//	6.查询所有
	private static void all() {
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] != null) {
				for (int j = 0; j < book[i].length; j++) {
					
					System.out.print(book[i][j]+"\t");
			}
			System.out.println();
		}	
	}
}
	
	private static void pubmanage() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key = sc.nextInt();
		switch (key) {
		case 1:
			pubadd();
			break;
		case 2:
			pubdelete();
			break;
		case 3:
			pubupdate();
			break;
		default:
			break;
		}
		
	}

	private static void pubadd() {
		
		int index = -1 ;
		for (int i = 0; i < pub.length; i++) {
			if (pub[i][0] == null) {
				index = i;
				break;
			}
		}

		while(true) {
			System.out.println("请输入出版社名称:");
			String pubname = sc.next();
			boolean result = pubnamecheck(pubname);
			if (result) {
				System.out.println("添加失败！！！该出版社已经存在！！！");
			}else {
				pub[index][0] = pubname;
				System.out.println("请输入出版社地址:");
				pub[index][1] = sc.next();
				System.out.println("请输入出版联系人:");
				pub[index][2] = sc.next();
				System.out.println("添加成功！！！");
				break;
			}
		}
	}
	
	private static void pubdelete() {
		
		System.out.println("请输入要删除的出版社名称：");
		String pubname = sc.next();
		int index = -1 ;
		
		boolean result = pubnamecheck(pubname);
		for (int i = 0; i < pub.length; i++) {
			if (result) {
				index = i;
				pub[index][0] = null;
				pub[index][1] = null;
				pub[index][2] = null;
				
				System.out.println("删除成功！！！");
				pubmanage();
				break;
			}else {
				System.out.println("没有找到该出版社!");
				System.out.println("删除失败");
				pubmanage();
				break;
			}
		}
	}
	
	private static boolean pubnamecheck(String pubName) {
		boolean Flag = false;
		for (int i = 0; i < book.length; i++) {
			if (pubName.equals(book[i][1])) {
				Flag = true;
				break;
			}
		}
		return Flag;
	}
	

	private static void pubupdate() {
		
		System.out.println("请输入出版社名：");
		String pubname = sc.next();
		
		for (int i = 0; i < pub.length; i++) {
			if (pub[i][0]!= null) {
				if (pub[i][0].equals(pubname)) {
					System.out.println("请输入新的出版社地址:");
					pub[i][1] = sc.next();
					System.out.println("请定义新的出版社联系人:");
					pub[i][2] = sc.next();
					System.out.println("更新成功！！！");
					pubmanage() ;
					break;
				}
			}else {
				System.out.println("该出版社不存在！");
				pubmanage() ;
			}
		}
	}
	
	public static void books() {
//		ISBN 书名 价格 出版社 作者
		book[0][0] = "10086";
		book[0][1] = "诈骗教学";
		book[0][2] = "100";
		book[0][3] = "南海出版社公司";
		book[0][4] = "电信客服";
		
		book[1][0] = "1";
		book[1][1] = "1";
		book[1][2] = "1";
		book[1][3] = "1";
		book[1][4] = "1";
		
	}
	
	private static void users() {
//		部门、用户名、密码、用户角色
		user[0][0] = "技术部";
		user[0][1] = "admin";
		user[0][2] = "123";
		user[0][3] = "vip";

		user[1][0] = "技术部";
		user[1][1] = "a";
		user[1][2] = "123";
		user[1][3] = "fw";

	}

	private static void pub() {
		
		pub[0][0] = "南海出版社";
		pub[0][1] = "南海";
		pub[0][2] = "fw";

		pub[1][0] = "东京出版社";
		pub[1][1] = "东京";
		pub[1][2] = "nb";
		
		pub[2][0] = "1";
		pub[2][1] = "1";
		pub[2][2] = "1";
	}

}
		


	
