package Lesson;

import java.util.Iterator;
import java.util.Scanner;

public class Demo1 {
		static	String users [][]=new String [20][5];
		static	Scanner scan=new Scanner(System.in);
		static String loginuse="";
		static String books [][]=new String [20][5];
		static String pressOL [][]=new String[20][5];
		public static void main(String[] args) {
			init();
			
			System.out.println("欢迎来到闽大图书馆");
			System.out.println("1.登录，2.注册");
			int key =scan.nextInt();
			switch (key) {
			case 1:
			loginview(); 
			
			break;
			case 2:
				registered();
			}
		
		}
		private static void loginview() {
			
			for (int i = 0; i < 3; i++) {
				System.out.println("请输入用户名");
				String usename=scan.next();
				System.out.println("请输入密码");
				String usepassword=scan.next();
				boolean flag=login(usename,usepassword);
				if(flag) {
					System.out.println("登录成功");
					loginuse=usename;
					homepage();
					menu();
					break;
				}
				else {
					System.out.println("不正确提示“该用户不存在或者密码错误！请重新登录！");
				}
			}
			}	
		public static void homepage() {
			System.out.println(loginuse+"正确，进入系统菜单");
		}
		public static boolean login(String usename,String usepassword) {
			boolean flag=false;
			for (int i = 0; i < users.length; i++) {
				if (usename.equals(users[i][1])&&usepassword.equals(users[i][2])) {
					flag=true;
					init();
				}
			}
			return flag;
		}
		
		public static void init () {
			users [0][0]="软件部";
			users [0][1]="admin";
			users [0][2]="123";
			users [0][3]="普通用户";
		}
		public static void registered(){
			System.out.println("请输入所属部门");
			String department =scan.next();
			System.out.println("请输入用户名");
			String username=scan.next();
			System.out.println("请输入密码");
			String userpassword=scan.next();
			for (int i = 0; i < users.length; i++) {
				if (users[i][0] == null) {
					users[i][0]=department;
					users[i][1]=username;
					users[i][2]=userpassword;
					
				}
				
			}
			System.out.println("注册成功!");
			System.out.println("欢迎来到闽大图书馆");
			System.out.println("1.登录，2.注册");
			int key=scan.nextInt();
			switch (key) {
			case 1:
			loginview(); 
			break;
			case 2:
				registered();
			}
			
		}
		public static void menu() {
			System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
			int key =scan.nextInt();
			switch (key) {
			case 1:
				bookmanage(); 
			break;
			case 2:
				press();
				break;
			case 3:
				logout();
				break;
			case 4:
				exit();
				break;
			}
			
		}
		private static void exit() {
			//退出系统
			System.out.println("");
			System.exit(0);
		}
		private static void logout() {
			System.out.println("已退出登录！");
			main(null);
			
		}
		private static void press() {
			//出版社管理
			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int key =scan.nextInt();
			switch (key) {
			case 1:
			pressadd(); 
			break;
			case 2:
			pressDelete();
				break;
			case 3:
				logout();
				break;
			case 4:
				exit();
				break;
			case 5:
				menu();
			}
		}
		private static void pressDelete() {
			pressOL();
			System.out.println("请输入要删除的出版社：");
			String pressname = scan.next();
			for (int i = 0; i < pressOL.length; i++) {
				if(pressname.equals(pressOL[i][1])) {
					pressOL[i][0] = null;
					pressOL[i][1] = null;
					pressOL[i][2] = null;
					pressOL[i][3] = null;
					pressOL[i][4] = null;
					System.out.println(i);
				}
			}
			System.out.println("删除成功");
			pressOL();
			
		}
		private static void pressadd() {
			pressOL();
			System.out.println("请输入出版社名称:");
			String pressName=scan.next();
			System.out.println("请输入出版社地址：");
	        String pressDress = scan.next();
	        System.out.println("请输入出版社联系人：");
	        String presscall= scan.next();
	        for (int i = 0; i < pressOL.length; i++) {
	            if (pressOL[i][0] == null) {
	            	pressOL[i][0] = pressName;
	            	pressOL[i][1] = pressDress;
	            	pressOL[i][2] = presscall;
	                System.out.println("添加出版社成功！");
	                menu();
	            }
	        }
			
		}
		private static void pressOL() {
			pressOL[0][0]="闽大出版社";
			pressOL[0][1]="闽大路";
			pressOL[0][2]="张三";
			
		}
		private static void bookmanage() {
			//图书管理系统
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int key =scan.nextInt();
			switch (key) {
			case 1:
			bookadd(); 
			break;
			case 2:
				bookDelete();
				break;
			case 3:
				logout();
				break;
			case 4:
				exit();
				break;
			case 5:
				menu();
			}
			
		}
		private static void bookDelete() {
			books();
			System.out.println("请输入要删除的书名：");
			String bookname = scan.next();
			for (int i = 0; i < books.length; i++) {
				if(bookname.equals(books[i][1])) {
					books[i][0] = null;
					books[i][1] = null;
					books[i][2] = null;
					books[i][3] = null;
					books[i][4] = null;
					System.out.println(i);
				}
			}
			System.out.println("删除成功");
			books();
				
				
			}
			
		
		private static void bookadd() {
			//添加图书
			books();
			System.out.println("请输入编码ISBN:");
			String bookNum=scan.next();
			System.out.println("请输入书籍名称：");
	        String bookName = scan.next();
	        System.out.println("请输入书籍价格：");
	        String bookPrice = scan.next();
	        System.out.println("请输入书籍出版社：");
	        String bookPress = scan.next();
	        System.out.println("请输入书籍作者：");
	        String bookWriter = scan.next();
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][0] == null) {
	                books[i][0] = bookNum;
	                books[i][1] = bookName;
	                books[i][2] = bookPrice;
	                books[i][3] = bookPress;
	                books[i][4] = bookWriter;
	               System.out.println("添加书籍成功！");
	               menu();
			
		}
	            
	        }
		
		}
		public static void books() {
			books[0][0] = "123";
	        books[0][1] = "Java基础";
	        books[0][2] = "9.9";
	        books[0][3] = "闽大出版社";
	        books[0][4] = "晨旭";
		}
		
		
}
