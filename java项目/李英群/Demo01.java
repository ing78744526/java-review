package Demo;

import java.util.Scanner;

public class Demo01 {
	static String[][] users = new String[50][5];
	static String[][] books = new String[50][7];
	static String[][] pubs = new String[50][5];
	public static void main(String[] args) {
		init();
		System.out.println("欢迎来到闽大图书管理系统！");
		Scanner sc = new Scanner(System.in);
		System.out.println("1登录  2注册");
		int key = sc.nextInt();
		
		switch (key) {
		case 1:
			for (int i=0;i<3;i++) {
				System.out.println("请输入用户名：");
				String usresname=sc.next();
				System.out.println("请输入登录密码：");
				String password=sc.next();
				
				boolean flag = login(usresname,password);
				if (flag) {
					System.out.println("登录成功！");
					
				}else {
					System.out.println("该用户不存在或者密码错误！请重新登录！");
				}
			}
			System.out.println("三次输入错误，系统自动退出!");

			break;

		default:
			break;
		}

	}
	
	
	
	private static boolean login(String usresname, String password) {
		boolean result = false;
		
		for (int i = 0; i < users.length; i++) {
			if (usresname.equals(users[i][1])&&password.equals(users[i][2])) {
				result = true;
			}
		}
		return result;
	}

	//初始化数据
	public static void init() {
		users[0][0]="软件部";
		users[0][1]="zhangsan";
		users[0][2]="123";
		users[0][3]="管理员";
		
		users[1][0]="行政部";
		users[1][1]="lisi";
		users[1][2]="111";
		users[1][3]="普通用户";
		
		books[0][0]="10001";
		books[0][1]="活着";
		books[0][2]="60";
		books[0][3]="南风出版社";
		books[0][4]="三";
		
		books[1][0]="10002";
		books[1][1]="特殊罪案调查组";
		books[1][2]="50";
		books[1][3]="浙江长风出版社";
		books[1][4]="九滴水";
		
		books[2][0]="10003";
		books[2][1]="尸语者";
		books[2][2]="45";
		books[2][3]="秦明出版社";
		books[2][4]="法医秦明";
		
		pubs[0][0]="南风出版社";
		pubs[0][1]="弄口巷88号";
		pubs[0][2]="三";
		
		pubs[1][0]="浙江长风出版社";
		pubs[1][1]="和平街53号";
		pubs[1][2]="九滴水";
		
		pubs[2][0]="秦明出版社";
		pubs[2][1]="平安街67号";
		pubs[2][2]="秦明";
		
	}

}
