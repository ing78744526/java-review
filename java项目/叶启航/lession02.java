package day06;
//用户信息（部门、用户名、密码、用户角色）

import java.util.Scanner;

public class lession01 {
	static String users[][]=new String [100][4];
	static String books[][]=new String [100][5];
	static String press[][]=new String [100][3];
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		users();
		books();
		press();
		loginn();
		
	}

	


	private static void loginn() {
		System.out.println("欢迎来到图书管理系统！");
		System.out.println("1.登入"+"2.注册");
		int key =scanner.nextInt();
		
		switch (key) {
		case 1:
			judge();
			break;
		case 2:
			registered();
		default:
			break;
		}
	}




	private static void registered() {
		System.out.println("请输入部门：");
		String department = scanner.next();
		System.out.println("请输入用户名：");
		String username = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();
		
		for (int i = 0; i < users.length; i++) {
			users[i][0]=department;
			users[i][1]=username;
			users[i][2]=password;
			users[i][3]="普通用户";
		}
		System.out.println("注册成功！");
		loginn();
	}

	private static void judge() {
		System.out.println("请输入用户名：");
		String username = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();
		users();
		boolean a =login(username, password);
		if (a) {
			System.out.println("登入成功！");
			welcome();
		}else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
			judge();
		}
		
		
	
	}


	private static void welcome() {
		System.out.println("欢迎您使用闽大书籍管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int k = scanner.nextInt();
		switch (k) {
		case 1://图书管理
			
			add();
			break;
		case 2://出版社管理
			
			add2();
			break;
		case 3:
			loginn();
			break;
		case 4:
			System.out.println("退出系统成功！");
			break;
		default:
			break;
		}
	}




	private static void add2() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 "
				+ "5.查询所有出版社 6.返回上一级菜单");
		int key1 = scanner.nextInt();
		switch (key1) {
		case 1:
			int index1 = -1;
			for (int i = 0; i < press.length; i++) {
				if (press[i][0]==null) {
					index1 = i;
				break;}
			}
			 
				System.out.println("请输入出版社名称：");
				press[index1][0] = scanner.next();
				System.out.println("请输入出版社地址：");
				press[index1][1] = scanner.next();
				System.out.println("请输入出版社联系人：");
				press[index1][2] = scanner.next();
				System.out.println("添加成功！");
				add2();
			break;
		case 2:
			del2();
			break;
		case 3:
			update2();
			break;
		case 4:
			
			select1();
			break;
		case 5:
			select2();
			break;
		case 6:
			welcome();
			break;
		default:
			break;
		}
		
		
	}




	private static void select2() {
		System.out.println("出版社名称\t   "+"地址\t"+"联系人\t");
		for (int i = 0; i < books.length; i++) {
			if (press[i][0]!=null) {
				System.out.println(press[i][0]+'\t'+"   "+press[i][1]+'\t'+press[i][2]);
				
			}
		}
		add2();
	}




	private static void select1() {
		boolean aaaa = false;
		System.out.println("请输入出版社名称：");
		String s3 = scanner.next();
		for (int i = 0; i < press.length; i++) {
			if (s3.equals(press[i][0])) {
				aaaa = true;
				System.out.println("出版社名称："+press[i][0]);
				System.out.println("出版社地址："+press[i][1]);
				System.out.println("出版社联系人："+press[i][2]);
				add2();
			}
		}
		if (aaaa) {
		}else {
			System.out.println("该出版社不存在！！！请重新查询！！！");
			add2();
		}
	}




	private static void update2() {
		boolean fla2 = false;
		System.out.println("请输入要更新的出版社名称：");
		String pressname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (pressname.equals(press[i][0])) {
				fla2=true;
				System.out.println("请输入要更新的地址：");
				press[i][1] = scanner.next();
				System.out.println("请输入要更新的联系人姓名：");
				press[i][2] = scanner.next();
				System.out.println("更新成功");
				add2();
			}
		}
		if (fla2) {
			
		
		}else {
			System.out.println("该出版社不存在！！！");
			update2();
		}
	}




	private static void del2() {
		boolean f = false; 
		System.out.println("请输入要删除的出版社名称：");
		String pressname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (pressname.equals(press[i][0])) {
				f=true;
				press[i][0] = null;
				press[i][1] = null;
				press[i][2] = null;
			}
		}
		if (f) {
			System.out.println("删除成功！");
			add2();
		}else {
			System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！\r\n"
					+ "删除失败");
			add2();
		}
		
	}




	private static void add() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int ke = scanner.nextInt();
		switch (ke) {
		case 1:
			int index = -1;
			for (int i = 0; i < books.length; i++) {
				if (books[i][0]==null) {
				index = i;
				break;
				}
				
			}
			
				System.out.println("请输入图书的ISBN:");
				books[index][0] = scanner.next();
				System.out.println("请输入书名:");
				books[index][1] = scanner.next();
				System.out.println("请输入价格:");
				books[index][2] = scanner.next();
				System.out.println("请输入出版社:");
				books[index][3] = scanner.next();
				System.out.println("请输入作者:");
				books[index][4] = scanner.next();
				System.out.println("添加成功！");
				add();
			break;
		case 2:
			del();
			break;
		case 3:
			update();
			break;
		case 4:
			select();
			break;
		case 5:
			welcome();
			break;
		default:
			break;
		}
	}




	private static void select() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int s = scanner.nextInt();
		
		switch (s) {
		case 1:
			boolean s1 =false;
			System.out.println("请输入ISBN号：");
			String bookisbn = scanner.next();
			for (int i = 0; i < books.length; i++) {
				if (bookisbn.equals(books[i][0])) {
					s1=true;
					System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
					break;
				}
			}
			for (int i = 0; i < books.length; i++) {
				if (bookisbn.equals(books[i][0])) {
					s1=true;
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
		
			if (s1) {
				select();
			}else {
				
				System.out.println("没有找到该ISBN号，请重新查询！");
				select();
			}
			break;
		case 2:
			boolean ss1 = false;
			System.out.println("请输入书名：");
			String bookname =scanner.next();
			for (int i = 0; i < books.length; i++) {
				if (books[i][1]!= null) {
					if (books[i][1].indexOf(bookname)!=-1) {
						ss1 = true;
						System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
						break;
					}
				}
				
			}
			for (int i = 0; i < books.length; i++) {
				if (books[i][1]!= null) {
					if (books[i][1].indexOf(bookname)!=-1) {
						ss1 = true;
						System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
						
					}
				}
				
			}
			
			if (ss1) {
				select();
			}else {
				System.out.println("没有找到该出版社，请重新查询！");
				select();
			}
			break;
		case 3:
			System.out.println("请输入出版社名称：");
			String pressname = scanner.next();
			boolean ff = false;
			for (int i = 0; i < books.length; i++) {
				if (pressname.equals(books[i][3])) {
					ff=true;
					System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
					break;
				}
			}
			for (int i = 0; i < books.length; i++) {
				if (pressname.equals(books[i][3])) {
					ff=true;
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
			
			if (ff) {
				select();
			}else {
				System.out.println("输入错误，请重新查询：");
				select();
			}
			
			break;
		case 4:
			books();
			boolean  s2 = false;
			System.out.println("请输入作者姓名：");
			String author = scanner.next();	
			for (int i = 0; i < books.length; i++) {
				if (author.equals(books[i][4])) {
					s2=true;
					System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
					break;
				}
			}
			for (int i = 0; i < books.length; i++) {
				if (author.equals(books[i][4])) {
					s2=true;
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
			
			if (s2) {
				select();
			}else {
				System.out.println("没有找到该作者，请重新查询！");
				select();
			}
			break;
		case 5:
			System.out.println("请输入最低价格：");
			int minprice = scanner.nextInt();
			System.out.println("请输入最高价格：");
			int maxprice = scanner.nextInt();
			System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
			for (int i = 0; i < books.length; i++) {
				
				if (books[i][0]!=null) {
					int price =Integer.parseInt(books[i][2]);
					if (price>minprice && price<maxprice) {
						
						System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
						
					}
				}
			}
			select();
			break;
		case 6:
				System.out.println("ISBN\t"+"书名\t"+"价格\t"+"出版社\t"+"作者\t");
			for (int i = 0; i < books.length; i++) {
				if (books[i][0]!=null) {
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
					
				}
			}
			select();
			break;		
		case 7:
			add();
			break;
		default:
			break;
		}
	}




	private static void update() {
		boolean fla = false;
		System.out.println("请输入ISBN号：");
		String bookisbn = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (bookisbn.equals(books[i][0])) {
				fla=true;
				System.out.println("请输入新的书名：");
				books[i][1] = scanner.next();
				System.out.println("请输入新的价格：");
				books[i][2] = scanner.next();
				System.out.println("请输入新的出版社：");
				books[i][3] = scanner.next();
				System.out.println("请输入新的作者：");
				books[i][4] = scanner.next();
				System.out.println("更新成功");
				add();
			}
		}
		if (fla) {
			
		}else {
			System.out.println("该ISBN号不存在！！！");
			add();
		}
	}




	private static void del() {
		boolean flag = false; 
		System.out.println("请输入你要删除的图书的名字");
		String bookname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (bookname.equals(books[i][1])) {
				flag=true;
				books[i][0] = null;
				books[i][1] = null;
				books[i][2] = null;
				books[i][3] = null;
				books[i][4] = null;
				
			}
		}
		if (flag) {
			System.out.println("删除成功！");
			add();
		}else {
			System.out.println("没有找到该书！\r\n" + "删除失败");
			add();
		}
		
	}




	private static boolean login(String username,String password) {
		boolean a =false;
		
		for (int i = 0; i < users.length; i++) {
			if (username.equals(users[i][1]) && password.equals(users[i][2])) {
				a = true;
			}
		}
		return a;
	}			
		


	private static void users() {
		users[0][0] = "技术部";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "管理员";
		
		users[1][0] = "行政部";
		users[1][1] = "ye";
		users[1][2] = "123";
		users[1][3] = "管理员";
		
		users[2][0] = "流通部";
		users[2][1] = "yin";
		users[2][2] = "123";
		users[2][3] = "管理员";
		
		users[3][0] = "采编部";
		users[3][1] = "123";
		users[3][2] = "123";
		users[3][3] = "管理员";
	}
	//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
		private static void books() {
			books[0][0] = "100001";
			books[0][1] = "JAVA";
			books[0][2] = "80";
			books[0][3] = "中华书局";
			books[0][4] = "司马迁";
			
			books[1][0] = "100002";
			books[1][1] = "HTML";
			books[1][2] = "90";
			books[1][3] = "作家出版社";
			books[1][4] = "余华";
			
			books[2][0] = "100003";
			books[2][1] = "SQL";
			books[2][2] = "100";
			books[2][3] = "重庆出版社";
			books[2][4] = "刘慈欣";
			
			books[3][0] = "100004";
			books[3][1] = "C#";
			books[3][2] = "88";
			books[3][3] = "南海出版社";
			books[3][4] = "东野";
		}
		//出版社名称、地址、联系人
		private static void press() {
			press[0][0] = "中华书局";
			press[0][1] = "东8路";
			press[0][2] = "11";
			
			press[1][0] = "作家出版社";
			press[1][1] = "东7路";
			press[1][2] = "22";
			
			press[2][0] = "重庆出版社";
			press[2][1] = "东6路";
			press[2][2] = "33";
			
			press[3][0] = "南海出版社";
			press[3][1] = "东5路";
			press[3][2] = "44";
		}
}
