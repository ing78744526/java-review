import java.util.Scanner;

public class Demo01 {
	static String[][] users = new String[100][4];
	static Scanner scanner = new Scanner(System.in);
	static String loginUser = "";

	public static void main(String[] args) {
		init();
		index();
		System.out.println("欢迎来到闽大图书馆！");
		System.out.println("1.登入   2.注册");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			loginModule();
			break;
		case 2:
			System.out.println("请输入部门：");
			String deptName = scanner.next();
			System.out.println("请输入用户名：");
			String UserName = scanner.next();
			System.out.println("请输入密码：");
			String PassWord = scanner.next();
			
			int UserIndex = getFirstNullUserIndex();
			users[UserIndex][0] = deptName;
			users[UserIndex][1] = UserName;
			users[UserIndex][2] = PassWord;
			users[UserIndex][3] = "普通用户";
			System.out.println("注册成功！");
			index();
			break;
			
		default:
			break;
		}
	}
	private static void index() {
		// TODO Auto-generated method stub
		
	}
	private static int getFirstNullUserIndex() {
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0]==null) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	private static void loginModule() {
		for (int i = 0; i < users.length; i++) {
			System.out.println("请输入用户名：");
			String UserName = scanner.next();
			System.out.println("请输入密码：");
			String PassWord = scanner.next();

			boolean flag = login(UserName, PassWord);
			if (flag) {
				System.out.println("登入成功！");
				loginUser = loginUser;
				homePage();
			} else {
				System.out.println("输入错误！");
			}
		}
		System.out.println("输入错误超过三次，系统自动退出！");
	}

	public static void homePage() {
		System.out.println(loginUser + "登入成功！欢迎使用闽大图书管理系统！");
	}

	public static boolean login(String UserName, String PassWord) {
		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (UserName.equals(users[i][1]) && PassWord.equals(users[i][2])) {
				flag = true;
			}
		}
		return flag;
	}
	public static void init() {
		users[0][0] = "软件部";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "管理员";

		users[1][0] = "行政部";
		users[1][1] = "张三";
		users[1][2] = "123";
		users[1][3] = "普通用户";

	}

}
