package sdgljn.md.com;

public class Demo03 {

	public static void main(String[] args) {
		int a[]= {10,20,30,40,50,60};
		name1(a);
		name2(a);
		int aa=name1(a);
		int bb=name2(a);
		System.out.println("最大值:"+aa);
		System.out.println("最小值:"+bb);
	}
	public static int name1(int arr[]) {
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max<arr[i]) {
				max=arr[i];
			}
		}
		return max;
	}
	public static int name2(int arr[]) {
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (min>arr[i]) {
				min=arr[i];
			}
		}
		return min;
	}
/**
 * - 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
- 注意: return语句, 只能带回一个结果.
- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
 */
}
