package zy.zmx.wu;

public class Xxq2 {

		public static void main(String[] args) {
			//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
			//- 注意: return语句, 只能带回一个结果.
			//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
					int []arr = {60,50,70,80,90,40};
					
					int max = getmax(arr);
					int min = getmin(arr);
				}
				public static int getmax(int[] arr) {
					int max;
					max = arr[0];
					
					for (int i = 0; i < arr.length; i++) {
						if (arr[i]>max) {
							max=arr[i];
						}
					}
					System.out.println("数组中元素的最大值是:"+max);
					return 0;
					
				}
				public static int getmin(int[] arr) {
					int min;
					min = arr[0];
					
					for (int i = 0; i < arr.length; i++) {
						if (arr[i]<min) {
							min=arr[i];
						}
					}
					System.out.println("数组中元素的最小值是:"+min);
					return 0;
					
					
				}

			}
