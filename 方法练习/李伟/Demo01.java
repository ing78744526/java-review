package cc;

public class Demo01 {
//	需求：设计一个方法用于数组遍历，要求遍历的结果是在一行上的。例如：[11, 22, 33, 44, 55] 
	public static void main (String[] args) {
		int [] a= {11,22,33,44,55};
		rund(a);
	}
	public static void rund (int a[]) {
		for (int i= 0;i < a.length;i++) {
			if(i==0) {
				System.out.print("[");
			}
			System.out.print(a[i]+" ");
			if(i>=0&&i<a.length-1) {
				System.out.print(",");
			}
			if(i==a.length-1) {
				System.out.print("]");
			}
			}
	}
}

	