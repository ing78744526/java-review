package wyf;

public class lazy02 {
//	- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//	- 注意: return语句, 只能带回一个结果.

//	- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
	public static void main(String[] args) {
		int crr[]= {66,77,88,99};
		int err[]=rund(crr);
		for (int i = 0; i < err.length; i++) {
			System.out.println(err[i]);
		}
	}
	public static int []rund(int crr[]) {
		int arr[]= new int [2];
		int max=crr[0];
		int min=crr[0];
		for (int j = 0; j < crr.length; j++) {
			if(crr[j]>max) {
				max=crr[j];
			}
			if(crr[j]<min) {
				min=crr[j];
			}
		}
		arr[0]=max;
		arr[1]=min;
		return arr;
	}
}
