package ccc;

import java.util.*;

public class Test {
	public static void main(String[] args) {
		
		// 1
		int arr[] = new int[5];
		Scanner scanner = new Scanner(System.in);
		
		for(int i = 0; i < arr.length; i++) {
			System.out.println("please input a number:");
			int a = scanner.nextInt();
			arr[i] = a;
		}
		Loop(arr);
		System.out.println();
		
		// 2
		System.out.println(Max(arr));
		
		// 3
		int[] second = new int[2];
		System.out.println("max: " + MAXIN(arr,second)[0] + ", min: " + MAXIN(arr,second)[1]);
		
		// 4
		int ran[] = new int[7];
		Ran(ran);
		System.out.println();
		
		// 5
		arr = Array(arr);
		Loop(arr);
		
		System.out.println();
		System.out.println("insert a num");
		int b = scanner.nextInt();
		int arr2[] = new int[arr.length + 1];
		for (int i = 0; i < arr2.length; i++) {
			if (i == arr.length) {
				arr2[i] = b;
			} else {
				arr2[i] = arr[i];
			} 
		}
		
		arr2 = Array(arr2);
		Loop(arr2);
		
		
	}

	public static void Loop (int[] arr) {
		System.out.print("��");
		for(int i = 0; i < arr.length; i++) {
			if(i == arr.length - 1) {
				System.out.print(arr[i]);
			}else {
				System.out.print(arr[i] + ", ");
			}
		}
		System.out.print("��");
	}
	
	public static int Max (int arr[]) {
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max < arr[i]) max = arr[i];
		}
		return max;
	}
	
	public static int[] MAXIN(int arr[], int[] second) {
		int max = arr[0];
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max < arr[i]) max = arr[i];
			if (min > arr[i]) min = arr[i];
		}
		
		second[0] = max;
		second[1] = min;
		return second;
	}
	
	public static void Ran (int ran[]) {
		Random random = new Random();
		for (int i = 0; i < ran.length; i++) {
			int num = random.nextInt(9);
			ran[i] = num;
		}
		
		for (int i = 0; i < ran.length; i++) {
			if(i != 4) {
				System.out.print(ran[i]);
			} else if (i == 4) {
				System.out.print(" " + ran[i]);
			}
		}
	}
	
	public static int[] Array (int[] array) {
		 for(int i = 0; i < array.length; i++) {
				int temp = i;
				for(int j = i; j < array.length; j++) {
					if(array[j] > array[temp]) {
						temp = j;
					}
				}
				int tempArr = array[i];
				array[i] = array[temp];
				array[temp] = tempArr;
			}
		return array;
		
	}
	
}
