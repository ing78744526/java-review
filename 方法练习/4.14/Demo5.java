package yccccc;

import java.util.Arrays;

/**
 * 	如果一个数组保存元素是有序的（从大到小），向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。
 * @author Administrator
 *
 */
public class Demo5 {  
 public static void main(String[] args) {
	int[] numbers = new int[] {1,5,24,25,56,0};
	int num =6;
	int index =numbers.length-1;
	for (int i = 0; i < numbers.length; i++) {
		if (numbers[i]>num) {
			index = i;
			break;
		}
	}
	for (int i = numbers.length-1;i>index;i--) {
		numbers[i] = numbers[i-1];
	}
	numbers[index] = num;
	System.err.println(Arrays.toString(numbers));
}
}