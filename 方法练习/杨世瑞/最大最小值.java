package java复习;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class 最大最小值 {
	public static void main(String[] args) {
		int[] arr = new int[5];
		Scanner sj = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i + 1) + "个数字：");
			arr[i] = sj.nextInt();
		}
		int bs[] = new int[2];
		bs = ff(arr);
		System.out.println(Arrays.toString(bs));
	}

	public static int[] ff(int[] arr) {
		int max = arr[0];
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
			;
		}
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i];
			}
			;
		}
		int bs[] = { max, min };
		return bs;
	}

}
//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//- 注意: return语句, 只能带回一个结果.
//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值