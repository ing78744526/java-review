package Demo;
/*
 * - 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

- 注意: return语句, 只能带回一个结果.

- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
 */
public class Demo44 {
	public static void main(String[] args) {
		int[] arr = {55,85,59,58,214};
		int[] num = getmain(arr);
		System.out.println("最大值"+num[0]);
		System.out.println("最小值"+num[1]);
	}
	public static int[] getmain(int[] arr) {
		int[] num = new int[2];
		int max = arr[0];
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max<arr[i]) {
				max=arr[i];
			}
			if (min>arr[i])  {
			    min=arr[i];
			}
			num[0] = max;
			num[1] =min;
		}
		return num;
	}
}
