package com.day2.demo;

import java.util.Arrays;
import java.util.Scanner;

public class Demo5 {
	//	如果一个数组保存元素是有序的（从大到小），向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。
	public static void main(String[] args) {
		int[] arr = {50,40,30,20,10};
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入您要插入的数字：");
		int num = sc.nextInt();
		int[] newArr=rankArray(num,arr);
		
		System.out.println(Arrays.toString(newArr));

	}

	private static int[] rankArray(int num, int[] arr) {
		int[] newArr=new int[arr.length+1];
		newArr[arr.length]=num;
		int temp;
		int index=-1;
		for (int i = 0; i < arr.length; i++) {
			if (num>arr[i]){
				index=i;
				break;
			}else{
				index=newArr.length-1;
			}
		}

		for (int i = 0; i < newArr.length; i++) {
			if (i<index){
				newArr[i]=arr[i];
			}else if(i>index){
				newArr[i]=arr[i-1];
			}else{
				newArr[index]=num;
			}
		}
		return newArr;
	}
}
