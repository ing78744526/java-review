import java.util.jar.Attributes.Name;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import org.omg.CORBA.PUBLIC_MEMBER;

/*
		 *  需求：设计一个方法用于获取数组中元素的最大值 
		 *  * 思路：

  * ①定义一个数组，用静态初始化完成数组元素初始化
  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
  * ③调用获取最大值方法，用变量接收返回结果
  * ④把结果输出在控制台
		 */
public class Qifei03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int []a= {10,20,30,40,50};
		int num = Name(a);
		System.out.println("最大值"+num);
	}
	public static int Name(int[] a) {
		int max = a[0]; 
		for (int i = 0; i < a.length; i++) {
			if (max<a[i]) {
				max=a[i];
			}
		}return max;
	}
	}