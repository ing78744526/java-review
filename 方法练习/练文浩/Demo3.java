package Lesson;

import java.util.Scanner;

public class Demo3 {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		int arr[]=new int [6];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入数组中的数");
			arr[i]=scan.nextInt();
		}/*- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

	- 注意: return语句, 只能带回一个结果.

	- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值*/
		
		int a[]=getmain(arr);
		int max=a[0];
		int min=a[1];
		System.out.println("最大值："+max+"最小值："+min);
	}
	public static int[] getmain (int arr[]) {
		int max=arr[0];
		int min=arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max<arr[i]) {
				max=arr[i];
			}
			if (min>arr[i]) {
				min=arr[i];
			}
		}
		int a[]= {max,min};
		return a;
	}
}
