package Demo;

public class Demo03 {

	public static void main(String[] args) {
// - 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//- 注意: return语句, 只能带回一个结果.
//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
		int[] arr= {11, 22, 33, 44, 55};
	
		int max = Getmax(arr);
		int min = Getmin(arr);

	}
	public static int Getmax(int[] arr) {
		int max;
		max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		System.out.println("数组的最大值:"+max);
		return 0;
	}
	public static int Getmin(int[] arr) {
		int min;
		min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		System.out.println("数组的最小值:"+min);
		return 0;
	}

}
