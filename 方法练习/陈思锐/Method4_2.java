package com.md.review2;

import java.util.Random;

//定义一个方法，实现的功能是：
//随机生成银行卡后7位数，返回的数据格式如：0586 756

public class Method4_2 {
	public static void main(String[] args) {
		
		String s = account();
		System.out.println(s);
	}
	
	public static String account() {
		
		Random r = new Random();
		
		String a = "0000" + r.nextInt(10000);
		String b = "000" + r.nextInt(10000);
		
		String s = a.substring(a.length() - 4, a.length()) + " " +
				   b.substring(b.length() - 3, b.length());
		

		System.out.println(s);
		
		return "";
	}
}
