package com.md.review2;

import java.util.Random;

public class Method5_2 {

	public static void main(String[] args) {
		int[] arr = {7,6,5,4,3,1};
		
		int[] newArr = newArr(arr);
		
		for(int i = 0;i < newArr.length;i++) {
			System.out.print(newArr[i] + "|");
		}

	}
	
	public static int[] newArr(int[] arr) {
		
		Random r = new Random(); 
		
		int newNum = r.nextInt(10);
		
		int[] newArr = new int[arr.length + 1];
		
		int index = arr.length - 1;
		for(int i = 0;i < arr.length;i++) {
			if(newNum > arr[i]) {
				index = i;
				break;
			}
		}
		
		for(int i = 0;i < newArr.length;i++) {
			if(i < index) {
				newArr[i] = arr[i];
			}else if(i == index) {
				newArr[i] = newNum;
			}else {
				newArr[i] = arr[i- 1];
			}
		}
		
		return newArr;
	}

}
