package DAY03;

import java.util.Scanner;

/*/
 * 接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工， 每个员工的考勤项有
 * 上下班忘打卡、 迟到、 早退、 旷工， 其中上下班忘打卡扣款10元/次，
 *  迟到和早退扣款为20元/次， 旷工100元/天， 参考图如下(参考图中是以3个员工为例)： 
 */
public class DEMO04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arr[][] = new int[4][5];
		for (int i = 0; i < 4; i++) {
			System.out.println("请输入第"+(i+1)+"位员工编号：");
			arr[i][0] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工的忘记打卡次数：");
			arr[i][1]= scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工的迟到次数：");
			arr[i][2] = scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工的早退次数：");
			arr[i][3]= scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"位员工的旷工次数：");
			arr[i][4]= scanner.nextInt();
		}
		System.out.println("***************本月考勤信息***************");
		System.out.println("员工编号"+'\t'+"忘打卡"+'\t'+"迟到"+'\t'+"早退"+'\t'+"旷工"+'\t'+"总罚款");
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 5; j++) {
				if (j==4) {
					System.out.println(""+arr[i][j]+'\t'+((arr[i][1]*10)+(arr[i][2]*20)+(arr[i][3]*20)+(arr[i][4]*100)));
				}
				else {
					System.out.print(""+arr[i][j]+'\t');
				}
				
				
			}
		}
		
	}
}
