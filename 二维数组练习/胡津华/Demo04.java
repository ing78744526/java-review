package lession;

import java.util.Scanner;

/*
 * 接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工， 
 * 每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工， 其中上下班忘打卡扣款10元/次，
 *  迟到和早退扣款为20元/次，
 *  旷工100元/天， 参考图如下(参考图中是以3个员工为例)： 
 */
public class Demo01 {

	public static void main(String[] args) {
		int [][]arr= new int[5][5];
		Scanner scan= new Scanner (System.in);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
		     System.out.println("输入员工"+(i+1)+"的编号");
		          arr[i][0] =scan.nextInt();
		          System.out.println("输入员工"+(i+1)+"的忘卡次数");
		          arr[i][1] =scan.nextInt();
		          System.out.println("输入员工"+(i+1)+"的迟到次数");
		          arr[i][2] =scan.nextInt();
		          System.out.println("输入员工"+(i+1)+"的早退次数");
		          arr[i][3] =scan.nextInt();
		          System.out.println("输入员工"+(i+1)+"的旷工次数次数");
		          arr[i][4] =scan.nextInt();
		          break;
			}
		}
			int[] sum = new int[5];
			for (int i = 0; i< arr.length; i++) {
				sum[i] +=arr[i][1]*10;
				sum[i] +=arr[i][2]*20;
				sum[i] +=arr[i][3]*20;
				sum[i] +=arr[i][4]*100;
				arr[i][5]=sum[i];
				
			}
			System.out.println("*********"+"本月考勤信息"+"**********");
			System.out.println("员工编号"+"\t"+"忘打卡次数"+"\t"+"的迟到次数"+"\t"+"的早退次数"+"\t"+"的旷工次数次数"+"\t"+"总罚款");
			for (int i = 0; i < 5; i++) {
				System.out.println();
				for (int j = 0; j < 6; j++) {
					System.out.println(arr[i][j]+"\t");
				}
			}
		}

		}



