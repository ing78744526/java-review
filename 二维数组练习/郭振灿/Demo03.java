package Demo;

public class Demo03 {
	   public static void main(String[] args) {
	   int[] arr= {19,28,37,46,50};
	   //调用数组反转方法
	   revArray(arr);
	   //调用数组遍历方法输出数组
	   printArray(arr);
	 }
	 
	 public static void revArray(int[] arr) {
	 //将两个条件放在一起
	  for(int i=0,j=arr.length-1;i<=j;i++,j--) {
	   int t=arr[i];
	   arr[i]=arr[j];
	   arr[j]=t;
	   }
	 }
	 
	 public static void printArray(int[] arr) {
	 //在数组元素前加符号'{'
	  System.out.print("{");
	  for(int x=0;x<arr.length;x++) {
	  //最后一个元素后不需要加','需要加符号'}'
	   if(x!=arr.length-1) {
	   System.out.print(arr[x]+",");
	  }else {
		  System.out.println(arr[x]+"}");
	     }
	   }
	  }
	}
	