package AA;

import java.lang.reflect.Array;
import java.util.Arrays;

public class AA2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*定义一个方法，用来实现如下功能：
​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素*/
		int[]arr= {19, 28, 37, 46, 50};
		System.out.println("交换后的数组 "+Arrays.toString(name(arr)));
	}
	public static int[] name(int[] arr) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return arr;
	}
}
