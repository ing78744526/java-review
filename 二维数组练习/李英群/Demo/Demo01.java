package Demo;

import java.util.Arrays;
import java.util.Scanner;

public class Demo01 {

	public static void main(String[] args) {
//用方法实现数组的排序。
//定义一个函数，实现数组从大到小排序，
//传入一个乱序数组，返回一个从大到小排序的数组。
		int[] arr = {70,30,50,40,91};
		int[] newArr = sortArray(arr);
		for (int i = 0; i < newArr.length; i++) {
			System.out.println(newArr[i]);
		}
		System.out.println("从大到小排序"+Arrays.toString(sortArray(arr)));
		
	}
	public static int[] sortArray(int[] arr) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return arr;
	}

}
