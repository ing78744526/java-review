import java.util.Arrays;
import java.util.Scanner;

public class 冒号排序 {

	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
        int arr[] = new int [5];
        for (int i = 0; i < arr.length; i++) {
			System.out.println("输入第"+(i+1)+"个数:");
        	arr[i] = scan.nextInt();
		}
        System.out.println("原始排序:");
        System.out.println(Arrays.toString(arr));
        System.out.println(" ");
        System.out.println("排序过程:");
        //冒泡
        for (int i = 0; i < arr.length-1; i++) {
            //外层循环，遍历次数
            for (int j = 0; j < arr.length - i - 1; j++) {
                //内层循环，升序（如果前一个值比后一个值大，则交换）
                //内层循环一次，获取一个最大值
                if (arr[j] < arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }
}
