package CyX;

public class AnArraYofExchange {
	 public static void main(String[] args) {
	        // 定义数组
	        int[] arr = {19, 28, 37, 46, 50};
	        // 调用数组反转方法
	        exchange(arr);
	        // 调用遍历数组方法
	        traverse(arr);
	    }

	    // 数组反转方法
	    public static void exchange(int[] arr) {
	        // 循环遍历数组
	        for (int start = 0, end = arr.length - 1; start <= end; start++, end--) {
	            // 变量交换
	            int temp = arr[start];
	            arr[start] = arr[end];
	            arr[end] = temp;
	        }
	    }

	    // 遍历数组方法
	    public static void traverse(int[] arr) {
	        System.out.print("[");
	        for (int i = 0; i < arr.length; i++) {
	            if (i == arr.length - 1) {
	                System.out.print(arr[i]);
	            } else {
	                System.out.print(arr[i] + ", ");
	            }
	        }
	        System.out.print("]");
	    }
	}


