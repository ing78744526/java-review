package com.md.lession2;

import java.util.Arrays;

public class Demo02 {

	public static void main(String[] args) {
	//用方法实现数组的排序。
	//定义一个函数，实现数组从大到小排序，
	//传入一个乱序数组，返回一个从大到小排序的数组。
		int []arr = {60,50,80,70,90};
		sortArray(arr);
		System.out.println("从大到小排序为："+Arrays.toString(sortArray(arr)));

	}
	public static int[] sortArray(int[] arr) {
		for (int i = 0; i < 5-1; i++) {//i<arr.length-1,也可以
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		return arr;
	}

}
