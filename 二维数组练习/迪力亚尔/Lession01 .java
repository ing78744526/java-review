package com.md.lession2;

import java.util.Arrays;

public class Lession01 {

	public static void main(String[] args) {
		int[] arr= {19, 28, 37, 46, 50};
		changeArray(arr);
	}
	public static void changeArray(int[] arr) {
		for (int i = 0; i < arr.length/2; i++) {
			int temp=arr[arr.length-1-i];
			arr[arr.length-1-i]=arr[i];
			arr[i]=temp;
		}
		System.out.println("交换后:"+Arrays.toString(arr));
	}
}
