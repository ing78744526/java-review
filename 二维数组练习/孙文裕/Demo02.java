import java.util.Arrays;

public class Demo02 {
	public static void main(String[] args) {
		int [] a = {20,50,89,60,67};
		System.out.println(Arrays.toString(changNum(a)));
	}
	public static int[] changNum(int[] arr) {
		for (int i = 0; i < 5-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int tamp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = tamp;
				}
			}
		}
		return arr;
	}
}
