package Demo;

public class Demo02 {
	// 定义一个方法，用来实现如下功能：
	//已知一个数组 arr = {19, 28, 37, 46, 50};
	//用程序实现把数组中的元素值交换，
	//交换后的数组 arr = {50, 46, 37, 28, 19};
	//并在控制台输出交换后的数组元素

	public static void main(String[] args) {
		int[] arr = {19, 28, 37, 46, 50};
//		int temp = arr[0];
//		arr[0]=arr[arr.length-1];
//		arr[arr.length-1]=temp;
		
//	    temp = arr[1];
//		arr[1]=arr[arr.length-2];
//		arr[arr.length-2]=temp;
		
		for (int i = 0; i < arr.length/2; i++) {
			int temp = arr[i];
			arr[i]=arr[arr.length-1-i];
			arr[arr.length-1-i]=temp;
			
		}
		
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+",");
		}
	}
}
