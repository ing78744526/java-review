package GG.kk.LBW;

import java.util.Scanner;
/*
已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
​最终输出a = 20，b = 10;
*/
public class Demo03 {
	public static void main(String[] args) {
		int a = 10;
		int b = 20;
		int c = 0 ;
		c = a ;
		a = b ;
		b = c ;
		System.out.println(a);
		System.out.println();
	}
}
