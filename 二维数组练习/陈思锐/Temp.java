package com.md.review3;

import java.util.Arrays;

public class Temp {

	public static void main(String[] args) {
		int[] arr = {19,28,37,46,50};
		
		arr = temp(arr);
		
		
		
		System.out.print(Arrays.toString(arr));
		
	}
	
	public static int[] temp(int[] arr) {
		
		int temp;
		
		for(int i = 0; i < 2 ; i ++) {
			temp = arr[arr.length - 1 - i];
			arr[arr.length - 1 - i] = arr[i];
			arr[i] = temp;
		}
		
		
		return arr;
	}

}
