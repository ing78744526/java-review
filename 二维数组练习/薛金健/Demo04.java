package sa;

public class Demo04 {
	//已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
		//​        最终输出a = 20，b = 10;
		public static void main(String[] args) {
		int []a= {10};
		int []b= {20};
		System.out.println("未交换时");
		System.out.println("a的值"+a[0]);
		System.out.println("b的值"+b[0]);
		for (int i = 0; i < b.length; i++) {
		if (a[0]>b[0]||a[0]<b[0]) {
			int temp=a[0];
			a[0]=b[0];
			b[0]=temp;
		}
		
		}
		System.out.println("交换后");
		System.out.println("a的值"+a[0]);
		System.out.println("b的值"+b[0]);
		}
		
}
