package student;

import java.util.Scanner;

public class Demo02 {
	/*
	 * -   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
                  选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。

-   思路：
        1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
        2.键盘录入评委分数
        3.由于是6个评委打分，所以，接收评委分数的操作，用循环
        4.求出数组最大值
        5.求出数组最小值
        6.求出数组总和
        7.按照计算规则进行计算得到平均分
        8.输出平均分
	 * 
	 */
  public static void main(String[] args) {
	  int arr []=new int[6];

   Scanner scanner=new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入得分");
			arr[i]=scanner.nextInt();
			if (arr[i]>=0&&arr[i]<=100) {
			
			}else {
				System.out.println("成绩输入有误！请重新输入！");
				i--;
			}
		}
		int max=arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		int min =arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		int sum =0;
		for (int i = 0; i < arr.length; i++) {
			sum=sum+arr[i];
		}
	System.out.println(max);
	System.out.println(min);
	System.out.println(sum);
		int avg ;
		avg=(sum-max-min)/4;
	System.out.println("平均分为"+avg);
	  
}
	

}
