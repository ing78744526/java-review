package com.md.Lession;

import java.util.Scanner;

public class Demo3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arr [] = new int [6];
		int max = arr[0];
		int min = arr[0];
		int sum = 0 ;
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入你的评分：");
			arr[i] = scanner.nextInt();
			if(arr[i]>=0&&arr[i]<=100) {
				sum += arr[i];
			}else {
				System.out.println("你输入的分数有误！请重新输入！");
				i = (arr.length+1);
			}
			if(arr[i]>max) {
				max = arr[i];
			}
			if(arr[i]<min) {
				min = arr[i];
			}
		}
		System.out.println("最大分："+max);
		System.out.println("最小分："+min);
		System.out.println("总分："+sum);
		System.out.println("平均分："+(sum-max-min)/4);
	}
}
