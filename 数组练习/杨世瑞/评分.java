package 评分;

import java.util.Scanner;

public class 评分 {

	public static void main(String[] args) {
		// TODO 自动生成的方法存根
		Scanner sc = new Scanner(System.in);
		float[] arr = new float[6];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i + 1) + "位评委的评分：");
			float fs = sc.nextFloat();
			arr[i] = fs;
		}
		float max = arr[0];
		float min = arr[0];
		int x = 0;
		int y = 0;
		for (int i = 0; i < arr.length; i++) {
			if (max < arr[i]) {
				max = arr[i];
				x = i;
			}
		}
		System.out.println("最高分：" + max);
		for (int i = 0; i < arr.length; i++) {
			if (min > arr[i]) {
				min = arr[i];
				y = i;
			}
		}
		System.out.println("最低分：" + min);
		float sum = 0;
		float avg = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		System.out.println("总分：" + sum);
		arr[y] = 0;
		arr[x] = 0;
		for (int i = 0; i < arr.length; i++) {
			avg += arr[i];
		}
		System.out.println("平均分：" + avg / 4);

	}

}

//-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
//选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。
//
//-   思路：
//1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
//2.键盘录入评委分数
//3.由于是6个评委打分，所以，接收评委分数的操作，用循环
//4.求出数组最大值
//5.求出数组最小值
//6.求出数组总和
//7.按照计算规则进行计算得到平均分
//8.输出平均分