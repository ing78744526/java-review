package com.md.cyx;

import java.util.Iterator;
import java.util.Scanner;

public class JudgesAppraisal {
	
	public static void main(String[] args) {
	
		int[] arrScore = new int[6];
		
		Scanner scan = new Scanner(System.in);
		
		for (int i = 0; i < arrScore.length; i++) {
			System.out.println("请评委为第"+ (i + 1) +"位选手打分：");
		    arrScore[i] = scan.nextInt();
			
		}
		int sum = 0 ;
		int max=arrScore[0];
		int min=arrScore[0];
		
		for (int i = 0; i < arrScore.length; i++) {
			if ( arrScore[i] > max ) {
				max = arrScore[i];
			}
			if ( arrScore[i] < min ) {
				min = arrScore[i];
			}
			sum += arrScore[i];
		}
		System.out.println("选手的最终得分为:"+(sum - max - min)/4.0);
	}
}
